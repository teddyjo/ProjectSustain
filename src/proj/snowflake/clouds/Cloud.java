package proj.snowflake.clouds;

public class Cloud {
	public double x;
	public double y;
	
	public Cloud(double x, double y) {
		this.x = x;
		this.y = y;
	}
}