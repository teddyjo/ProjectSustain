package proj.snowflake.misc;

public class MunnyParticle {
	public double x;
	public double y;
	public String identifier;
	public long birthday;
	
	public MunnyParticle(int x, int y, String i) {
		this.x = x;
		this.y = y;
		this.identifier = i;
		birthday = System.currentTimeMillis();
	}
}
