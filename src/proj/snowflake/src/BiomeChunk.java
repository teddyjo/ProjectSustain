package proj.snowflake.src;

public class BiomeChunk extends Chunk {
	public BiomeChunk() {
		
	}
	/*
	 * INDEX KEY:
	 * 0 - Desert
	 * 1 - Meadow
	 * 2 - Temperate
	 * 3 - Shrub land
	 * 4 - Permafrost
	 * */
	public int index = 0;
	
	public BiomeChunk(int x, int y) {
		this.x = x;
		this.y = y;
		index = (int) (Math.random() * 100)+1;
		index = 50;
		if(index < 20) {
			dirtLevel = 30;
			stoneLevel = dirtLevel + (int) (Math.random() * 2 + 1);
			aquifersPerChunk = 3;
			coalVeinsPerChunk = 30;
			AquiferStartingPoint[] aquifers = new AquiferStartingPoint[aquifersPerChunk];
			CoalStartingPoint[] coalVeins = new CoalStartingPoint[coalVeinsPerChunk];
			for(int i = 0; i < blocks.length; i++) {
				for(int j = 0; j < blocks[0].length; j++) {
					if(j > dirtLevel && j <= stoneLevel) {if(j == dirtLevel + 1) {
							blocks[i][j] = new SandBlock();
						}
						else if(j == dirtLevel + 2) {
							if(Math.random() >= .5) {
								blocks[i][j] = new SandBlock();
							}
							else {
								blocks[i][j] = new SandBlock();
							}
						}
						else {
							blocks[i][j] = new SandBlock();
						}
					}
					else if(j > stoneLevel) {
						//System.out.println((((float)j)/((float)stoneLevel))/3);
						if(Math.random() < (((float)j + 55)/((float)stoneLevel))/3) {
							blocks[i][j] = new StoneBlock();
						}
						else {
							blocks[i][j] = new SandBlock();
						}
					}
					else {
						blocks[i][j] = null;
					}
				}
			}
			
			for(int i = 0; i < aquifersPerChunk; i++) {
				aquifers[i] = new AquiferStartingPoint(this);
			}
			
			for(int i = 0; i < coalVeinsPerChunk; i++) {
				coalVeins[i] = new CoalStartingPoint(this);
			}
		}
		else if(index >= 20 && index < 40) {
			dirtLevel = 30;
			stoneLevel = dirtLevel + (int) (Math.random() * 5 + 8);
			aquifersPerChunk = 8;
			coalVeinsPerChunk = 8;
			AquiferStartingPoint[] aquifers = new AquiferStartingPoint[aquifersPerChunk];
			CoalStartingPoint[] coalVeins = new CoalStartingPoint[coalVeinsPerChunk];
			for(int i = 0; i < blocks.length; i++) {
				for(int j = 0; j < blocks[0].length; j++) {
					if(j > dirtLevel && j <= stoneLevel) {if(j == dirtLevel + 1) {
							blocks[i][j] = new LimestoneBlock();
						}
						else if(j == dirtLevel + 2) {
							if(Math.random() >= .5) {
								blocks[i][j] = new DirtBlock();
							}
							else {
								blocks[i][j] = new DirtBlock();
							}
						}
						else {
							blocks[i][j] = new DirtBlock();
						}
					}
					else if(j > stoneLevel) {
						if(Math.random() < (((float)j + 55)/((float)stoneLevel))/3) {
							blocks[i][j] = new StoneBlock();
						}
						else {
							blocks[i][j] = new DirtBlock();
						}
					}
					else {
						blocks[i][j] = null;
					}
				}
			}
			
			for(int i = 0; i < aquifersPerChunk; i++) {
				aquifers[i] = new AquiferStartingPoint(this);
			}
			
			for(int i = 0; i < coalVeinsPerChunk; i++) {
				coalVeins[i] = new CoalStartingPoint(this);
			}
		}
		else if(index >= 40 && index < 60) {
			dirtLevel = 30;
			stoneLevel = dirtLevel + (int) (Math.random() * 5 + 4);
			aquifersPerChunk = 15;
			coalVeinsPerChunk = 15;
			AquiferStartingPoint[] aquifers = new AquiferStartingPoint[aquifersPerChunk];
			CoalStartingPoint[] coalVeins = new CoalStartingPoint[coalVeinsPerChunk];
			for(int i = 0; i < blocks.length; i++) {
				for(int j = 0; j < blocks[0].length; j++) {
					if(j > dirtLevel && j <= stoneLevel) {if(j == dirtLevel + 1) {
							blocks[i][j] = new LimestoneBlock();
						}
						else if(j == dirtLevel + 2) {
							if(Math.random() >= .5) {
								blocks[i][j] = new LimestoneBlock();
							}
							else {
								blocks[i][j] = new DirtBlock();
							}
						}
						else {
							blocks[i][j] = new DirtBlock();
						}
					}
					else if(j > stoneLevel) {
						if(Math.random() < (((float)j + 55)/((float)stoneLevel))/3) {
							blocks[i][j] = new StoneBlock();
						}
						else {
							blocks[i][j] = new DirtBlock();
						}
					}
					else {
						blocks[i][j] = null;
					}
				}
			}
			
			for(int i = 0; i < aquifersPerChunk; i++) {
				aquifers[i] = new AquiferStartingPoint(this);
			}
			
			for(int i = 0; i < coalVeinsPerChunk; i++) {
				coalVeins[i] = new CoalStartingPoint(this);
			}
		}
		else if(index >= 60 && index < 80) {
			dirtLevel = 30;
			stoneLevel = dirtLevel + (int) (Math.random() * 3 + 6);
			aquifersPerChunk = 20;
			coalVeinsPerChunk = 10;
			AquiferStartingPoint[] aquifers = new AquiferStartingPoint[aquifersPerChunk];
			CoalStartingPoint[] coalVeins = new CoalStartingPoint[coalVeinsPerChunk];
			for(int i = 0; i < blocks.length; i++) {
				for(int j = 0; j < blocks[0].length; j++) {
					if(j > dirtLevel && j <= stoneLevel) {if(j == dirtLevel + 1) {
							blocks[i][j] = new LimestoneBlock();
						}
						else if(j == dirtLevel + 2) {
							if(Math.random() >= .5) {
								blocks[i][j] = new LimestoneBlock();
							}
							else {
								blocks[i][j] = new DirtBlock();
							}
						}
						else {
							if(Math.random() > (((float)j)/((float)stoneLevel))/3) {
								blocks[i][j] = new DirtBlock();
							}
							else {
								blocks[i][j] = new StoneBlock();
							}
						}
					}
					else if(j > stoneLevel) {
						blocks[i][j] = new StoneBlock();
					}
					else {
						blocks[i][j] = null;
					}
				}
			}
			
			for(int i = 0; i < aquifersPerChunk; i++) {
				aquifers[i] = new AquiferStartingPoint(this);
			}
			
			for(int i = 0; i < coalVeinsPerChunk; i++) {
				coalVeins[i] = new CoalStartingPoint(this);
			}
		}
		else {
			dirtLevel = 30;
			stoneLevel = dirtLevel + (int) (Math.random() * 2 + 40);
			aquifersPerChunk = 3;
			coalVeinsPerChunk = 13;
			AquiferStartingPoint[] aquifers = new AquiferStartingPoint[aquifersPerChunk];
			CoalStartingPoint[] coalVeins = new CoalStartingPoint[coalVeinsPerChunk];
			for(int i = 0; i < blocks.length; i++) {
				for(int j = 0; j < blocks[0].length; j++) {
					if(j > dirtLevel && j <= stoneLevel) {
						if(j == dirtLevel + 1) {
							blocks[i][j] = new PermaGrassBlock();
						}
						else if(j == dirtLevel + 2) {
							if(Math.random() >= .5) {
								blocks[i][j] = new PermaGrassBlock();
							}
							else {
								blocks[i][j] = new PermaJordBlock();
							}
						}
						else if(j >= stoneLevel - 7) {
							if(Math.random() < (((float)j)/((float)stoneLevel + 36)) && Math.random() <= (((float)j)/((float)stoneLevel))) {
								blocks[i][j] = new PermaJordBlock();
							}
							else if(Math.random() > (((float)j)/((float)stoneLevel))) {
								blocks[i][j] = new PermaGrassBlock();
							}
							else {
								blocks[i][j] = new StoneBlock();
							}
						}
						else {
							//System.out.println((((float)j)/((float)stoneLevel)));
							if(Math.random() <= (((float)j)/((float)stoneLevel))) {
								blocks[i][j] = new PermaJordBlock();
							}
							else {
								blocks[i][j] = new PermaGrassBlock();
							}
						}
					}
					/*else if(j > stoneLevel) {
						if(Math.random() < 0.9) {
							blocks[i][j] = new StoneBlock();
						}
						else {
							blocks[i][j] = new PermaJordBlock();
						}
					}*/
					else if(j > stoneLevel) {
						if(Math.random() > (((float)j)/((float)stoneLevel + 36)) && Math.random() <= (((float)j)/((float)stoneLevel))) {
							blocks[i][j] = new PermaJordBlock();
						}
						else if(Math.random() > (((float)j)/((float)stoneLevel))) {
							blocks[i][j] = new PermaGrassBlock();
						}
						else {
							blocks[i][j] = new StoneBlock();
						}
					}
					else {
						blocks[i][j] = null;
					}
				}
			}
			
			for(int i = 0; i < aquifersPerChunk; i++) {
				aquifers[i] = new AquiferStartingPoint(this);
			}
			
			for(int i = 0; i < coalVeinsPerChunk; i++) {
				coalVeins[i] = new CoalStartingPoint(this);
			}
		}
	}
}
