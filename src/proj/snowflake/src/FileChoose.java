package proj.snowflake.src;

import java.awt.Canvas;

import javax.swing.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.*;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.text.DecimalFormat;

public class FileChoose extends Canvas implements Runnable, KeyListener, ActionListener, MouseListener {

    public static final long serialVersionUID = 1L;
    public boolean isRunning = false;
    Font font = new Font("Arial", Font.PLAIN, 20);
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final String NAME = "Game";
    DecimalFormat df = new DecimalFormat("#.##");

    public JFrame jFrame;
    
    File file = new File(".");

    // Reading directory contents
    File[] files = file.listFiles();
    ArrayList<FileInfo> fileInfo = new ArrayList<FileInfo>();
    
    Main m;

    public FileChoose(Main m) {
    	this.m = m;
    	for (int i = 0; i < files.length; i++) {
            if(files[i].toString().contains(".psave")) {
            	fileInfo.add(new FileInfo(files[i].toString()));
            }
            
        }
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setMaximumSize(new Dimension(WIDTH, HEIGHT));

        jFrame = new JFrame(NAME);
        //jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(new BorderLayout());
        jFrame.add(this, BorderLayout.CENTER);
        jFrame.setResizable(true);
        jFrame.setLocation(1366/2 - WIDTH/2,768/2 - HEIGHT/2);
        jFrame.setVisible(true);
        jFrame.addKeyListener(this);
        addKeyListener(this);
        jFrame.addMouseListener(this);
        addMouseListener(this);
        jFrame.pack();
        
        start();
    }

    public synchronized void start() {
        isRunning = true;
        new Thread(this).start();
    }

    public synchronized void stop() {
        isRunning = false;
    }

    public void run() {
        while (isRunning) {
            //System.out.println("Hej");
            tick();
            render();
        }
    }

    public void tick() {
        
    }
    public void render() {
         BufferStrategy bs = getBufferStrategy();
        if(bs == null) {
            createBufferStrategy(3);
            return;
        }
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        g.clearRect(0, 0, WIDTH, HEIGHT);
        g.setFont(font);
        g.setColor(Color.BLACK);
        //g.fillRect(100, 100, 100, 100);
        for(int i = 0; i < fileInfo.size(); i++) {
        	if(fileInfo.get(i).grade.length() > 0) {
        		g.drawString("- " + fileInfo.get(i).name + ", Grade " + fileInfo.get(i).grade, 10, 20 + 20*i);
        	}
        	else {
        		g.drawString("- " + fileInfo.get(i).name, 10, 20 + 20*i);
        	}
        }
        g.drawString("Which saved game is yours?", 500, 100);
        g.drawString("Click on it!", 500, 130);
        g.dispose();
        bs.show();
    }

    public void keyTyped(KeyEvent e) {
        
    }

    public void keyPressed(KeyEvent e) {
        
    }

    public void keyReleased(KeyEvent e) {

    }

    public void actionPerformed(ActionEvent e) {
        
    }
    public void mousePressed(MouseEvent e) {
        int x = e.getY() / 20;
        try {
        	m.fileSave = fileInfo.get(x - 1).url;
        	m.load();
        } catch(Exception ie) {}
    }
    public void mouseReleased(MouseEvent e) {
        
    }
    public void mouseClicked(MouseEvent e) {
        
    }
    public void mouseEntered(MouseEvent e) {
        
    }
    public void mouseExited(MouseEvent e) {
        
    }

    public static void main(String args[]) {
        new Main().start();
    }
}