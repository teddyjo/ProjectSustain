package proj.snowflake.src;

public class FileInfo {
	public String url = "";
	public String name = "";
	public String grade = "";
	
	public FileInfo(String url) {
		this.url = url;
		for(int i = 0; i < url.length() - 6; i++) {
			char c = url.charAt(i);
        	boolean isDigit = (c >= '0' && c <= '9');
        	if(isDigit) {
        		grade += url.charAt(i);
        	}
        	else {
        		if(url.charAt(i) != '/' && url.charAt(i) != '.') {
        			if(url.charAt(i) == '_') {
        				name += " ";
        			}
        			else {
        				name += url.charAt(i);
        			}
        		}
        	}
		}
	}
}
