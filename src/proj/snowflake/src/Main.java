package proj.snowflake.src;

import java.awt.Canvas;

import javax.imageio.ImageIO;
import javax.swing.*;

import sun.audio.*;
import proj.snowflake.misc.*;
import proj.snowflake.src.buildings.*;
import proj.snowflake.src.guimanagers.BuildingMenu;
import proj.snowflake.src.guimanagers.BuildingUpgradeMenu;
import proj.snowflake.src.guimanagers.DevModeMenu;
import proj.snowflake.src.guimanagers.GUIComponent;
import proj.snowflake.src.guimanagers.MenuMenu;
import proj.snowflake.src.guimanagers.SaveLoader;
import proj.snowflake.src.guimanagers.StatsMenu;
import proj.snowflake.transporters.*;
import proj.snowflake.clouds.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.io.*;

public class Main extends Canvas implements Runnable, KeyListener, ActionListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private static final long serialVersionUID = 1L;
	private boolean isRunning = false;
	public static Font font = new Font("Comic Sans MS", Font.BOLD, 20);
	Font font2 = new Font("Comic Sans MS", Font.BOLD, 15);
	Font font3 = new Font("Comic Sans MS", Font.BOLD, 10);
	Font font4 = new Font("Comic Sans MS", Font.BOLD, 20);
	Font mini = new Font("Arial", Font.PLAIN, 15);
	private Toolkit tk = Toolkit.getDefaultToolkit();
	private int WIDTH2 = ((int) tk.getScreenSize().getWidth());
	private int HEIGHT2 = ((int) tk.getScreenSize().getHeight());
    //public int WIDTH = ((int) tk.getScreenSize().getWidth());
    //public int HEIGHT = ((int) tk.getScreenSize().getHeight());
	private int WIDTH = 1000;
	private int HEIGHT = 600;
    //public static final int WIDTH = 800;
    //public static final int HEIGHT = 600;
	private double HEIGHTSCALE = 0.5;
	private double THEOHEIGHTSCALE = 0.5;
	private int VIRTUALHEIGHT;
	private int VIRTUALWIDTH;
	private double MAXZOOM = 2;
	private double MINZOOM = 0.3;
	private static final String NAME = "Game";
	private DecimalFormat df = new DecimalFormat("#.##");
	public ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	
	boolean devMode = false;
	boolean scienceMode = false;
	
	long lastSave = System.currentTimeMillis();
	
	File file = new File(".");

    // Reading directory contents
    File[] files = file.listFiles();
    ArrayList<FileInfo> fileInfo = new ArrayList<FileInfo>();
	public String fileSave = "";
	
	NameInput ni;
	FileChoose fc;
	
	private long nextRain;
	private long nextDry;
	
	private long lastSwitch;
	
	private long dryDuration;
	private long rainDuration;
	
	public int waterLimit = 1000;
	
	private boolean raining = false;
	
	private Color currColor = new Color(32, 128, 255);
	
	private int rainx = 1000;
	private int rainy = 600;
    
	private int recycleCount;
    
	private long debtWarning = System.currentTimeMillis() - 3000;
    
	private ArrayList<String> warnings = new  ArrayList<String>();
	private long lastWarningUpdate = System.currentTimeMillis();
	private long startTime = System.currentTimeMillis();
    
	private int doThatShit(int i) {
    	int shitToDo = (int) (Math.random() * (buildings.size()));
    	if(shitToDo == i) {
    		shitToDo = doThatShit(i);
    	}
    	return shitToDo;
    }
    
	private long lastStarRecord = System.currentTimeMillis();
	private ArrayList<Integer> starRecord = new ArrayList<Integer>();
	private int calcAverageStarCount() {
    	int total = 0;
    	for(int i = 0; i < starRecord.size(); i++) {
    		total += starRecord.get(i);
    	}
    	int average = total/starRecord.size();
    	return average;
    }
    
	private ArrayList<Transporter> inGames = new ArrayList<Transporter>();
    
	private boolean doneBefore = false;
    
	private int powerPlantCount;
    
	private ArrayList<Transporter> trannies = new ArrayList<Transporter>();
    
	private int statPanelx = 0;
	private int statPanely = 0;
    
	private ArrayList<CO2Thingy> co2Thingies = new ArrayList<CO2Thingy>();
	private long lastCO2ThingyUpdate = System.currentTimeMillis();
    
	private ArrayList<plusElectric> electrics = new ArrayList<plusElectric>();
	private long lastElectricUpdate = System.currentTimeMillis();
	private double co2Thingyspeed = 1000;
    
	private ArrayList<Face> faces = new ArrayList<Face>();
	private long lastFaceUpdate = System.currentTimeMillis();
    
	private boolean tutorialMode = false;
	private int tutorialIndex = 0;
	private ArrayList<String> tutStrings = new ArrayList<String>();
    
    //Instantiate the CO2Thingy 14 blocks to the right for destruction in the center of the tree
	private ArrayList<CO2Thingy> backwards = new ArrayList<CO2Thingy>();
	private long lastBackwardsUpdate = System.currentTimeMillis();
    
	private ArrayList<MunnyParticle> munnythings = new ArrayList<MunnyParticle>();
	private long lastMunnyThingUpdate = System.currentTimeMillis();
    
	private long lastClean = System.currentTimeMillis();
    
	private double airCleanFactor = 0.0;
    
    private int happiness = 0;
    private int fossilFyools = 0;
    private int waters = 0;
    private int science = 0;
    private int population = 2;
    private double munnies = 150;
    private double energies = 0;
    private double pollution = 0;
    private int foodings = 0;
    
    private long lastUpdate = System.currentTimeMillis();
    
    private String statPanelTag = "";
    
    private int houseNumber = 0;
    
    private double foodPerCapita = 0;
    private double energyPerCapita = 0;
    
    private double popDeriv = 0;
    
    private int MAXHOUSECAPACITY = 100;
    
    private boolean mousedBuildingCol = true;
    
    private int popCap = 0;
    
    private int ffreq = 0;
    
    private long lastConsumptionUpdate = System.currentTimeMillis();
    
    public long amplitude = 60000;
    public long period = 900000;
    
    private double round(double x) {
    	if(x > 1) {
    		x = 1;
    	}
    	if(x < 0) {
    		x = 0;
    	}
    	return x;
    }
    
    private double roundTo25(double x) {
    	if(x > 25) {
    		x = 25;
    	}
    	if(x < 0) {
    		x = 0;
    	}
    	return x;
    }
    
    public int[] k = new int[5];
    
    private void calcHappiness() {
    	int intermediate1 = (int) roundTo25((100 - (taxes)/2)/2);
    	int intermediate2 = (int) (round(foodPerCapita)*25);
    	int intermediate3 = (int) (round(energyPerCapita)*15);
    	int intermediate4 = (int) (((100 - pollution))/4);
    	int intermediate5 = (int) ((3*waters)/population);
    	if(waters*6 > population) {
    		intermediate5 = 2;
    	}
    	
    	double i1 = intermediate1/(k[0] + 1);
    	double i2 = intermediate2/(k[1] + 1);
    	double i3 = intermediate3/(k[2] + 1);
    	double i4 = intermediate4/(k[3] + 1);
    	double i5 = intermediate5/(k[4] + 1);
    	
    	intermediate2 *= i4;
    	intermediate3 *= i3;
    	intermediate4 *= i1;
    	intermediate1 *= i2;
    	intermediate5 *= i5;
    	
    	happiness = intermediate1 + intermediate2 + intermediate3 + intermediate4 + intermediate5;
    	
    	happiness += popDeriv*4;
    	
    	if(happiness > 100) {
    		happiness = 100;
    	}
    	if(happiness < 0) {
    		happiness = 0;
    	}
    	
    	hLog += happiness + "x";
    }
    
    private void calculatePerCapitas() {
    	popCap = houseNumber * MAXHOUSECAPACITY + 5;
    	foodPerCapita = ((double) (foodings))/population;
    	energyPerCapita = energies/population;
    	popDeriv = (foodPerCapita * energyPerCapita);
    	if(foodPerCapita <= 2 && energyPerCapita <= 2) {
    		popDeriv = 0.5;
    	}
    	if(population > popCap) {
    		popDeriv /= 2;
    		happiness /= 2;
    	}
    	if(popDeriv > 5) {
    		popDeriv = 5;
    	}
    	if(waters < population/2) {
    		popDeriv /= 2;
    	}
    	if(foodPerCapita <= 2) {
    		popDeriv -= 3;
    		happiness = 10;
    	}
    	if(pollution >= 80) {
    		population -= 3;
    	}
    	if(foodPerCapita <= 2) {
    		population -= 3;
    	}
    	if(population < 2) {
    		population = 2;
    	}
    	if(population > Math.abs(popDeriv-2)+1)
    		population += popDeriv - 2;
    	if(popDeriv >= 2.0 && population <= 5) {
    		population += popDeriv;
    	}
    }
    
    private long lastCloudMove = System.currentTimeMillis();
    
    private void calcNewFoodings() {
    	if(foodings > population)
    		foodings -= population;
    }
    
    private void calcNewEnergies() {
    	if(energies > population)
    		energies -= population;
    }
    
    //InputStream buildingPlaceSound;
    //InputStream buildingDestroySound;
    //InputStream buildingBoopSound;
    
    private Image house;
    private Image barn;
    private Image factory;
    private Image lab;
    private Image solar;
    private Image recycle;
    private Image nuclear;
    private Image landfill;
    private Image waterCleaner;
    
    private Image gasCar;
    private Image electricCar;
    private Image lightRail;
    private Image bus;
    private Image gasCarLEFT;
    Image electricCarLEFT;
    Image lightRailRIGHT;
    Image busRIGHT;
    
    Image dirt;
    Image coal;
    Image grass;
    Image water;
    Image stone;
    Image dirtyWater;
    Image darkStone;
    Image darkDirt;
    Image sand;
    Image permaJord;
    Image permaGrass;
    
    Image CO2ThingyGREEN;
    Image CO2ThingyRED;
    Image plusElectric;
    
    BufferedImage MenuMenuIMG;
    BufferedImage BuildingMenuIMG;
    BufferedImage StatBarsIMG;
    
    BufferedImage rainIMG;
    
    BufferedImage fullStar;
    BufferedImage halfStar;
    
    BufferedImage tutorialButton;
    BufferedImage playButton;
    BufferedImage titleDisplay;
    BufferedImage BigCloud;
    BufferedImage SmallCloud;
    
    BufferedImage rightArrow;
    BufferedImage downArrow;
    
    Image REDBarn;
    Image REDFactory;
    Image REDHouse;
    Image REDLab;
    Image REDSolar;
    Image REDRecycle;
    Image REDNuclear;
    Image REDLandfill;
    Image REDWaterCleaner;
    Image REDTree;
    
    Image GREENBarn;
    Image GREENFactory;
    Image GREENHouse;
    Image GREENLab;
    Image GREENSolar;
    Image GREENRecycle;
    Image GREENNuclear;
    Image GREENLandfill;
    Image GREENWaterCleaner;
    Image GREENTree;
    
    Image Tree1;
    Image Tree2;
    Image Tree3;
    Image Tree4;
    Image Tree5;	
    Image Tree6;
    Image Tree7;
    
    
    Image SmileyFace;
    Image FrownyFace;
    
    int scienceLevel = 1;
    int gradReq = scienceLevel*20;
    
    Color backColor = new Color(32, 128, 255);
    Color rainColor = new Color(128, 128, 128);
    //Color backColor = new Color(200, 64, 40);
    //Color backColor = new Color(32, 255, 32);
    Color pollColor = new Color(88, 60, 41);
    
    public GameState state = GameState.MENU;
    
    public enum GameState {
    	MENU, GAME;
    }
    
    public int getColorFraction(int x1, int x2) {
    	int difference = x2 - x1;
    	double fraction = pollution/100;
    	return x1 + (int) (difference*fraction);
    }
    public int getColorFractionFromFraction(int x1, int x2, double fraction) {
    	int difference = x2 - x1;
    	return x1 + (int) (difference*fraction);
    }
    
    public double addInRange(int small, int large, double x1, double y1) {
    	double yolo = x1 + y1;
    	if(yolo < small) {
    		yolo = small;
    	}
    	if(yolo > large) {
    		yolo = large;
    	}
    	return yolo;
    }
    
    public void checkMousedBuildingCol() {
    	mousedBuildingCol = false;
		for(int i = 0; i < buildings.size(); i++) {
			if(buildings.get(i) instanceof PowerPlant || buildings.get(i) instanceof Mine || buildings.get(i) instanceof Nuclear) {
				if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
					if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 10 > buildings.get(i).x) {
						mousedBuildingCol = true;
						return;
					}
				}
				else {
					if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 5 > buildings.get(i).x) {
						mousedBuildingCol = true;
						return;
					}
				}
			}
			else {
				if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
					if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 10 > buildings.get(i).x) {
						mousedBuildingCol = true;
						return;
					}
				}
				else {
					if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 5 > buildings.get(i).x) {
						mousedBuildingCol = true;
						return;
					}
				}
			}
		}
    }
    
    ArrayList<Building> buildings = new ArrayList<Building>();
    ArrayList<MiningTurtle> turtles = new ArrayList<MiningTurtle>();
    ArrayList<WaterDirtyTurtle> watTurt = new ArrayList<WaterDirtyTurtle>();
    ArrayList<WaterDirtyTurtle> nucTurt = new ArrayList<WaterDirtyTurtle>();
    ArrayList<WaterCleanTurtle> cleanTurt = new ArrayList<WaterCleanTurtle>();
    ArrayList<WaterTurtle> drinkingTurtles = new ArrayList<WaterTurtle>();
    
    boolean drawBuildingGUI = false;
    
    GUIComponent guiCompo;
    
    Building mousedBuilding;
    
    double pricePerKWH = 0;
    
    Ticker ticker = new Ticker(this);
    
    long timeLine = 0;
    
    ArrayList<ArrayList<Chunk>> chunkRow = new ArrayList<ArrayList<Chunk>>();
    
    AquiferStartingPoint test001;
    
    ArrayList<Cloud> clouds = new ArrayList<Cloud>();
    
    //Building Log
    String bLog = "";
    //Removal Log
    String rLog = "";
    //Weather Log
    String wLog = "";
    
    //Quantities of coal and water
    int numCoal = 0;
    int numWat = 0;
    
    //Coal Log
    String cLog = "";
    //Water Log
    String aLog = "";
    //Stored Water Log
    String vALog = "";
    //Happiness Log
    String hLog = "";
    
    boolean rightHeld = false;
    boolean leftHeld = false;
    boolean upHeld = false;
    boolean downHeld = false;
    
    boolean zoomingIn = false;
    boolean zoomingOut = false;
    
    /*public void playPlaceSound() {
    	try {
    		//sound = getClass().getResourceAsStream("BuildingPlacedSound.wav");
    		buildingPlaceSound = getClass().getResourceAsStream("/project/snowflake/sounds/BuildingPlaceSound.wav");
    		AudioStream audioStream = new AudioStream(buildingPlaceSound);
    		AudioPlayer.player.start(audioStream);
    	} catch(Exception e) {System.out.println(e);}
    }
    public void playDestroySound() {
    	try {
    		//sound = getClass().getResourceAsStream("BuildingPlacedSound.wav");
    		buildingDestroySound = getClass().getResourceAsStream("/project/snowflake/sounds/gameboom2.wav");
    		AudioStream audioStream = new AudioStream(buildingPlaceSound);
    		AudioPlayer.player.start(audioStream);
    	} catch(Exception e) {System.out.println(e);}
    }
    public void playBoopSound() {
    	try {
    		//sound = getClass().getResourceAsStream("BuildingPlacedSound.wav");
    		buildingBoopSound = getClass().getResourceAsStream("/project/snowflake/sounds/gameplace.wav");
    		AudioStream audioStream = new AudioStream(buildingPlaceSound);
    		AudioPlayer.player.start(audioStream);
    	} catch(Exception e) {System.out.println(e);}
    }*/
    
    public double taxes = 50;
    private double houseWaterAllowance = 100;
    private double farmWaterAllowance = 100;
    //private double ppWaterAllowance = 100;
    
    int mouseX = 0;
    int mouseY = 0;

    int movementSpeed = 2;
    
    private JFrame jFrame;
    
    public String[] seperate(String home, char sep) {
    	String[] temp = new String[3];
    	temp[0] = "";
    	temp[1] = "";
    	temp[2] = "";
    	int currentIndex = 0;
    	for(int i = 0; i < home.length(); i++) {
    		if(home.charAt(i) != 'x') {
    			temp[currentIndex] += home.charAt(i);
    		}
    		else {
    			currentIndex += 1;
    		}
    	}
    	return temp;
    }
    
    
    public void save() {
    	try {
    		BufferedWriter out = new BufferedWriter(new FileWriter(fileSave));
    		out.write("" + taxes);
    		out.newLine();
    		out.write("" + ffreq);
    		out.newLine();
    		out.write("" + happiness);
    		out.newLine();
    		out.write("" + fossilFyools);
    		out.newLine();
    		out.write("" + waters);
    		out.newLine();
    		out.write("" + science);
    		out.newLine();
    		out.write("" + population);
    		out.newLine();
    		out.write("" + munnies);
    		out.newLine();
    		out.write("" + pollution);
    		out.newLine();
    		out.write("" + foodings);
    		out.newLine();
    		out.write("" + tutorialIndex);
    		out.newLine();
    		out.write("" + buildings.size());
    		out.newLine();
    		for(int i = 0; i < buildings.size();i++) {
    			if(buildings.get(i) instanceof Tree) {
    				out.write("0");
    				out.write("y" + "" + ((Tree)buildings.get(i)).treeType);
    			}
    			if(buildings.get(i) instanceof House) {
    				out.write("1");
    				out.write("y" + "" + ((House)buildings.get(i)).transportationType);
    			}
    			if(buildings.get(i) instanceof Mine) {
    				out.write("2");
    			}
    			if(buildings.get(i) instanceof PowerPlant) {
    				out.write("3");
    			}
    			if(buildings.get(i) instanceof ScienceFacility) {
    				out.write("4");
    			}
    			if(buildings.get(i) instanceof SolarPlant) {
    				out.write("5");
    			}
    			if(buildings.get(i) instanceof RecyclePlant) {
    				out.write("6");
    			}
    			if(buildings.get(i) instanceof Nuclear) {
    				out.write("7");
    			}
    			if(buildings.get(i) instanceof Landfill) {
    				out.write("8");
    			}
    			if(buildings.get(i) instanceof WaterCleaner) {
    				out.write("9");
    			}
    			out.write("x" + buildings.get(i).x);
    			out.write("x" + buildings.get(i).y);
    			out.newLine();
    		}
    		out.write("" + turtles.size());
    		out.newLine();
    		for(int i = 0; i < turtles.size(); i++) {
    			out.write("" + ((MiningTurtle)turtles.get(i)).x + "x" + turtles.get(i).y + "x" + turtles.get(i).rightIndex + "x" + turtles.get(i).leftIndex);
    			out.newLine();
    		}
    		out.write("" + watTurt.size());
    		out.newLine();
    		for(int i = 0; i < watTurt.size(); i++) {
    			out.write("" + ((WaterDirtyTurtle)watTurt.get(i)).x + "x" + ((WaterDirtyTurtle)watTurt.get(i)).y + "x" + watTurt.get(i).rightIndex + "x" + watTurt.get(i).leftIndex);
    			out.newLine();
    		}
    		out.write("" + nucTurt.size());
    		out.newLine();
    		for(int i = 0; i < nucTurt.size(); i++) {
    			out.write("" + ((WaterDirtyTurtle)nucTurt.get(i)).x + "x" + ((WaterDirtyTurtle)nucTurt.get(i)).y + "x" + nucTurt.get(i).rightIndex + "x" + nucTurt.get(i).leftIndex);
    			out.newLine();
    		}
    		out.write("" + cleanTurt.size());
    		out.newLine();
    		for(int i = 0; i < cleanTurt.size(); i++) {
    			out.write("" + ((WaterCleanTurtle)cleanTurt.get(i)).x + "x" + ((WaterCleanTurtle)cleanTurt.get(i)).y + "x" + cleanTurt.get(i).rightIndex + "x" + cleanTurt.get(i).leftIndex);
    			out.newLine();
    		}
    		out.write("" + drinkingTurtles.size());
    		out.newLine();
    		for(int i = 0; i < drinkingTurtles.size(); i++) {
    			out.write("" + ((WaterTurtle)drinkingTurtles.get(i)).x + "x" + ((WaterTurtle)drinkingTurtles.get(i)).y + "x" + drinkingTurtles.get(i).rightIndex + "x" + drinkingTurtles.get(i).leftIndex);
    			out.newLine();
    		}
    		for(int i = 0; i < chunks.size(); i++) {
            	for(int j = 0; j < chunks.get(i).blocks.length; j++) {
            		for(int k = 0; k < chunks.get(i).blocks[0].length; k++) {
            			if(chunks.get(i).blocks[j][k] != null){
            				if(chunks.get(i).blocks[j][k] instanceof LimestoneBlock) {
            					if(chunks.get(i).blocks[j][k] instanceof SandBlock) {
            						//g.drawImage(sand, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            						out.write("0");
            					}
            					else if(chunks.get(i).blocks[j][k] instanceof PermaJordBlock) {
            						//g.drawImage(permaJord, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            						out.write("1");
            					}
            					else if(chunks.get(i).blocks[j][k] instanceof PermaGrassBlock) {
            						//g.drawImage(permaGrass, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            						out.write("2");
            					}
            					else {
            						//g.drawImage(grass, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            						out.write("3");
            					}
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DirtBlock) {
            					//g.setColor(new Color(128, 64, 0));
            					//g.drawImage(dirt, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("4");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof StoneBlock) {
            					//g.drawImage(stone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("5");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof AquiferBlock && !(chunks.get(i).blocks[j][k] instanceof DiscoWaBlock)) {
            					//g.setColor(new Color(0, 0, 255));
            					//g.drawImage(water, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("6");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DirtyWaterBlock) {
            					//g.drawImage(dirtyWater, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("7");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof CoalBlock) {
            					//g.drawImage(coal, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("8");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DiscoFFBlock) {
            					//g.setColor(new Color(64, 64, 64));
            					//g.drawImage(darkStone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("9");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DiscoWaBlock) {
            					//g.setColor(new Color(0, 128, 0));
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DarkStoneBlock) {
            					//g.drawImage(darkStone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("10");
            				}
            				else if(chunks.get(i).blocks[j][k] instanceof DarkDirtBlock) {
            					//g.drawImage(darkDirt, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
            					out.write("11");
            				}
            			}
            			else {
            				out.write("12");
            			}
            			if(k == chunks.get(i).blocks[0].length - 1) {
        					
        				}
        				else {
        					out.write("x");
        				}
            		}
            		out.newLine();
            	}
            }
    		out.write(bLog);
    		out.newLine();
    		out.write(rLog);
    		out.newLine();
    		out.write(wLog);
    		out.newLine();
    		out.write(cLog);
    		out.newLine();
    		out.write(aLog);
    		out.newLine();
    		out.write("" + timeLine);
    		out.newLine();
    		out.write(vALog);
    		out.newLine();
    		out.write(hLog);
    		out.newLine();
    		out.close();
    	} catch(Exception e) {System.out.println("poop22");}
    	ni = null;
    }
    
    public void load() {
    	if(fc != null) {
    		fc.jFrame.dispose();
    		fc = null;
    		jFrame.toFront();
    	}
    	try {
    		BufferedReader in = new BufferedReader(new FileReader(fileSave));
    		taxes = Double.parseDouble(in.readLine());
    		ffreq = Integer.parseInt(in.readLine());
    		happiness = Integer.parseInt(in.readLine());
    		fossilFyools = Integer.parseInt(in.readLine());
    		waters = Integer.parseInt(in.readLine());
    		science = Integer.parseInt(in.readLine());
    		population = Integer.parseInt(in.readLine());
    		munnies = Double.parseDouble(in.readLine());
    		pollution = Double.parseDouble(in.readLine());
    		foodings = Integer.parseInt(in.readLine());
    		tutorialIndex = Integer.parseInt(in.readLine());
    		int tot = Integer.parseInt(in.readLine());
    		buildings.clear();
    		//BUILDING INDEX
    		for(int i = 0; i < tot; i++) {
    			String intermediate = in.readLine();
    			String[] nums = seperate(intermediate, 'x');
    			if(Integer.parseInt(nums[0].charAt(0) + "") == 0) {
    				String[] treee = seperate(nums[0], 'y');
    				//buildings.add(new Tree(1));
    				buildings.add(new Tree(Integer.parseInt(nums[0].charAt(2) + "")));
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0].charAt(0) + "") == 1) {
    				buildings.add(new House());
    				((House)buildings.get(buildings.size()-1)).transportationType = Integer.parseInt(nums[0].charAt(2) + "");
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 2) {
    				buildings.add(new Mine());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 3) {
    				buildings.add(new PowerPlant());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 4) {
    				buildings.add(new ScienceFacility());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 5) {
    				buildings.add(new SolarPlant());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 6) {
    				buildings.add(new RecyclePlant());
    				recycleCount += 1;
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 7) {
    				buildings.add(new Nuclear());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 8) {
    				buildings.add(new Landfill());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    			else if(Integer.parseInt(nums[0]) == 9) {
    				buildings.add(new WaterCleaner());
    				buildings.get(buildings.size() - 1).x = Integer.parseInt(nums[1]);
    				buildings.get(buildings.size() - 1).y = Integer.parseInt(nums[2]);
    			}
    		}
    		int to = Integer.parseInt(in.readLine());
    		turtles.clear();
    		for(int i = 0; i < to; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			turtles.add(new MiningTurtle(Integer.parseInt(nums[0]),Integer.parseInt(nums[1])));
    			turtles.get(turtles.size() - 1).rightIndex = Integer.parseInt(nums[2]);
    			turtles.get(turtles.size() - 1).leftIndex = Integer.parseInt(nums[3]);
    		}
    		int toto = Integer.parseInt(in.readLine());
    		watTurt.clear();
    		for(int i = 0; i < toto; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			watTurt.add(new WaterDirtyTurtle(Integer.parseInt(nums[0]),Integer.parseInt(nums[1])));
    			watTurt.get(watTurt.size() - 1).rightIndex = Integer.parseInt(nums[2]);
    			watTurt.get(watTurt.size() - 1).leftIndex = Integer.parseInt(nums[3]);
    		}
    		int tot0 = Integer.parseInt(in.readLine());
    		nucTurt.clear();
    		for(int i = 0; i < tot0; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			nucTurt.add(new WaterDirtyTurtle(Integer.parseInt(nums[0]),Integer.parseInt(nums[1])));
    			nucTurt.get(nucTurt.size() - 1).rightIndex = Integer.parseInt(nums[2]);
    			nucTurt.get(nucTurt.size() - 1).leftIndex = Integer.parseInt(nums[3]);
    		}
    		int t = Integer.parseInt(in.readLine());
    		cleanTurt.clear();
    		for(int i = 0; i < t; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			cleanTurt.add(new WaterCleanTurtle(Integer.parseInt(nums[0]),Integer.parseInt(nums[1])));
    			cleanTurt.get(cleanTurt.size() - 1).rightIndex = Integer.parseInt(nums[2]);
    			cleanTurt.get(cleanTurt.size() - 1).leftIndex = Integer.parseInt(nums[3]);
    		}
    		int t0 = Integer.parseInt(in.readLine());
    		drinkingTurtles.clear();
    		for(int i = 0; i < t0; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			//drinkingTurtles.add(new WaterTurtle(Integer.parseInt(nums[0]),Integer.parseInt(nums[1])));
    			//drinkingTurtles.get(cleanTurt.size() - 1).rightIndex = Integer.parseInt(nums[2]);
    			//drinkingTurtles.get(cleanTurt.size() - 1).leftIndex = Integer.parseInt(nums[3]);
    		}
    		for(int i = 0; i < chunks.get(0).blocks.length; i++) {
    			String intermediate = in.readLine();
    			String[] nums = intermediate.split("x");
    			for(int j = 0; j < nums.length; j++) {
    				if(Integer.parseInt(nums[j]) == 0) {
    					chunks.get(0).blocks[i][j] = new SandBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 1) {
    					chunks.get(0).blocks[i][j] = new PermaJordBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 2) {
    					chunks.get(0).blocks[i][j] = new PermaGrassBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 3) {
    					chunks.get(0).blocks[i][j] = new LimestoneBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 4) {
    					chunks.get(0).blocks[i][j] = new DirtBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 5) {
    					chunks.get(0).blocks[i][j] = new StoneBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 6) {
    					chunks.get(0).blocks[i][j] = new AquiferBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 7) {
    					chunks.get(0).blocks[i][j] = new DirtyWaterBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 8) {
    					chunks.get(0).blocks[i][j] = new CoalBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 9) {
    					chunks.get(0).blocks[i][j] = new DiscoFFBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 10) {
    					chunks.get(0).blocks[i][j] = new DarkStoneBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 11) {
    					chunks.get(0).blocks[i][j] = new DarkDirtBlock();
    				}
    				if(Integer.parseInt(nums[j]) == 12) {
    					chunks.get(0).blocks[i][j] = null;
    				}
    			}
    		}
    		inGames.clear();
    		munnythings.clear();
    		faces.clear();
    		electrics.clear();
    		co2Thingies.clear();
    		calculatePerCapitas();
    		bLog = in.readLine();
    		rLog = in.readLine();
    		wLog = in.readLine();
    		cLog = in.readLine();
    		aLog = in.readLine();
    		timeLine = Long.parseLong(in.readLine());
    		vALog = in.readLine();
    		hLog = in.readLine();
    		in.close();
    		System.gc();
    	} catch(Exception e) {System.out.println(e);}
    }
    
    public Main() {
    	k[0] = 10;
        k[1] = 20;
        k[2] = 20;
        k[3] = 35;
        k[4] = 10;
    	
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setMaximumSize(new Dimension(WIDTH, HEIGHT));

        jFrame = new JFrame(NAME);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(new BorderLayout());
        jFrame.add(this, BorderLayout.CENTER);
        jFrame.setResizable(true);
        jFrame.setLocation(WIDTH2/2 - WIDTH/2,HEIGHT2/2 - HEIGHT/2);
        //jFrame.setUndecorated(true);
        jFrame.setVisible(true);
        jFrame.addKeyListener(this);
        addKeyListener(this);
        jFrame.addMouseListener(this);
        addMouseListener(this);
        jFrame.addMouseMotionListener(this);
        addMouseMotionListener(this);
        jFrame.addMouseWheelListener(this);
        addMouseWheelListener(this);
        jFrame.pack();
        
        jFrame.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
                WIDTH = jFrame.getWidth();
                HEIGHT = jFrame.getHeight();
            }
            public void componentHidden(ComponentEvent arg0) {}
            public void componentMoved(ComponentEvent arg0) {}
            public void componentShown(ComponentEvent arg0) {}
        });
        
        tutStrings.add("Welcome to Project Snowflake! Use the arrow keys to move the camera");
        tutStrings.add("To begin building your city, you need to make some space.");
        tutStrings.add("Click on a tree or building to open the editor.");
        tutStrings.add("Click 'Remove' to remove the tree or building.");
        tutStrings.add("Once you have removed enough trees to make space, let's build something.");
        tutStrings.add("Start by building a house to keep people in.");
        tutStrings.add("Make sure to place the house over water so that the people don't get thirsty!");
        tutStrings.add("Now that we have houses, let's feed our citizens.");
        tutStrings.add("Click on the farm.");
        tutStrings.add("Make sure to place farms over water so that farmers can water their crops!");
        tutStrings.add("Now let's supply our houses with electricity.");
        tutStrings.add("Click on a building that has '+Electricity' in the description.");
        tutStrings.add("If your building needs it, place it over fossil fuels (black squares).");
        tutStrings.add("You can also build Labs to make more food and electricity with science.");
        tutStrings.add("Click on the statistics button to see what your citizens need.");
        tutStrings.add("If one of the bars isn't green, then people want more houses, electricity, food, or less pollution.");
        tutStrings.add("Click on the management button to changes the taxes and water usage for your citizens.");
        tutStrings.add("You will make more money by having higher taxes, but your citizens will be less happy.");
        tutStrings.add("Click on a house to see what kind of transportation it uses.");
        tutStrings.add("If you ever have very few resources, click on the change button to make the house more efficient.");
        tutStrings.add("The stars at the top indicate the happiness of your citizens.");
        tutStrings.add("Happiness is affected by food, living space, electricity supply, taxes, and pollution.");
        tutStrings.add("If your citizens don't have enough food, power, or houses, they will stop paying their taxes.");
        tutStrings.add("Try to balance your city to make five-star-happy citizens!");
        tutStrings.add("Oh, by the way, press the spacebar to save and Esc to load a game!");
        
        for(int i = (int) (Math.random() * 20); i > 0; i--) {
        	if(Math.random() >= 0.5) {
        		clouds.add(new StorCloud(Math.random() * WIDTH, Math.random() * HEIGHT));
        	}
        	else {
        		clouds.add(new LittleCloud(Math.random() * WIDTH, Math.random() * HEIGHT));
        	}
        }
        
        trannies.add(new GasCar(0, 0, 0));
        trannies.add(new ElectricCar(0, 0, 0));
        trannies.add(new Bus(0, 0, 0));
        trannies.add(new LightRail(0, 0, 0));
        
        try {
        	house = ImageIO.read(getClass().getResource("/project/snowflake/images/House.png"));
        	barn = ImageIO.read(getClass().getResource("/project/snowflake/images/Barn.png"));
        	factory = ImageIO.read(getClass().getResource("/project/snowflake/images/Factory.png"));
        	lab = ImageIO.read(getClass().getResource("/project/snowflake/images/Lab.png"));
        	solar = ImageIO.read(getClass().getResource("/project/snowflake/images/SolarPanels.png"));
        	recycle = ImageIO.read(getClass().getResource("/project/snowflake/images/Recycle.png"));
        	nuclear = ImageIO.read(getClass().getResource("/project/snowflake/images/Nuclear.png"));
        	landfill = ImageIO.read(getClass().getResource("/project/snowflake/images/Landfill.png"));
        	waterCleaner = ImageIO.read(getClass().getResource("/project/snowflake/images/WaterCleaner.png"));
        	dirt = ImageIO.read(getClass().getResource("/project/snowflake/images/Dirt.png"));
        	coal = ImageIO.read(getClass().getResource("/project/snowflake/images/NewCoal.png"));
        	grass = ImageIO.read(getClass().getResource("/project/snowflake/images/Grass.png"));
        	water = ImageIO.read(getClass().getResource("/project/snowflake/images/Water.png"));
        	stone = ImageIO.read(getClass().getResource("/project/snowflake/images/Stone.png"));
        	sand = ImageIO.read(getClass().getResource("/project/snowflake/images/Sand.png"));
        	rainIMG = ImageIO.read(getClass().getResource("/project/snowflake/images/Rain.png"));
        	permaJord = ImageIO.read(getClass().getResource("/project/snowflake/images/PermaJord.png"));
        	permaGrass = ImageIO.read(getClass().getResource("/project/snowflake/images/PermaGrass.png"));
        	dirtyWater = ImageIO.read(getClass().getResource("/project/snowflake/images/DirtyWater.png"));
        	darkStone = ImageIO.read(getClass().getResource("/project/snowflake/images/DarkStone.png"));
        	darkDirt = ImageIO.read(getClass().getResource("/project/snowflake/images/DarkDirt.png"));
        	MenuMenuIMG = ImageIO.read(getClass().getResource("/project/snowflake/images/MenuMenuIMG.png"));
        	BuildingMenuIMG = ImageIO.read(getClass().getResource("/project/snowflake/images/BuildingMenuIcon.png"));
        	StatBarsIMG = ImageIO.read(getClass().getResource("/project/snowflake/images/StatBarsIcon.png"));
        	fullStar = ImageIO.read(getClass().getResource("/project/snowflake/images/FullStar.png"));
        	halfStar = ImageIO.read(getClass().getResource("/project/snowflake/images/HalfStar.png"));
        	playButton = ImageIO.read(getClass().getResource("/project/snowflake/menuimages/PLAYButton.png"));
        	tutorialButton = ImageIO.read(getClass().getResource("/project/snowflake/menuimages/TUTORIALButton.png"));
        	titleDisplay = ImageIO.read(getClass().getResource("/project/snowflake/menuimages/TITLE.png"));
        	BigCloud = ImageIO.read(getClass().getResource("/project/snowflake/menuimages/BigCloud.png"));
        	SmallCloud = ImageIO.read(getClass().getResource("/project/snowflake/menuimages/LittleCloud.png"));
        	CO2ThingyGREEN = ImageIO.read(getClass().getResource("/project/snowflake/images/CO2GREEN.png"));
        	CO2ThingyRED = ImageIO.read(getClass().getResource("/project/snowflake/images/CO2RED.png"));
        	plusElectric = ImageIO.read(getClass().getResource("/project/snowflake/images/+E.png"));
        	
        	gasCar = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/GasCar.png"));
        	electricCar = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/ElectricCar.png"));
        	lightRail = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/LightRail.png"));
        	bus = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/Bus.png"));
        	gasCarLEFT = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/GasCarLEFT.png"));
        	electricCarLEFT = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/ElectricCarLEFT.png"));
        	lightRailRIGHT = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/LightRailRIGHT.png"));
        	busRIGHT = ImageIO.read(getClass().getResource("/project/snowflake/transportimages/BusRIGHT.png"));
        	
        	rightArrow = ImageIO.read(getClass().getResource("/project/snowflake/arrowimages/RightArrowYELLOW.png"));
        	downArrow = ImageIO.read(getClass().getResource("/project/snowflake/arrowimages/DownArrowYELLOW.png"));
        	
        	SmileyFace = ImageIO.read(getClass().getResource("/project/snowflake/images/SmileyFace.png"));
        	FrownyFace = ImageIO.read(getClass().getResource("/project/snowflake/images/FrownyFace.png"));
        	
        	
        	Tree1 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree1.png"));
        	Tree2 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree2.png"));
        	Tree3 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree3.png"));
        	Tree4 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree4.png"));
        	Tree5 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree5.png"));
        	Tree6 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree6.png"));
        	Tree7 = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree7.png"));
        	
        	IconHolder.houseIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/HouseIcon.png"));
        	IconHolder.barnIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/BarnIcon.png"));
        	IconHolder.factoryIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/FactoryIcon.png"));
        	IconHolder.labIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/LabIcon.png"));
        	if(scienceMode) {
        		IconHolder.solarIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/SolarGray.png"));
        		IconHolder.recycleIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/RecycleGray.png"));
        		IconHolder.nuclearIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/NuclearGray.png"));
        		IconHolder.landfillIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Landfill.png"));
        		IconHolder.cleanerIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/WaterCleanerGray.png"));
        		IconHolder.treeIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree1Gray.png"));
        	}
        	else {
        		IconHolder.solarIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/SolarIcon.png"));
            	IconHolder.recycleIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Recycle.png"));
            	IconHolder.nuclearIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Nuclear.png"));
            	IconHolder.landfillIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Landfill.png"));
            	IconHolder.cleanerIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/WaterCleaner.png"));
            	IconHolder.treeIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree1.png"));
        	}
        	
        	REDBarn = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/BarnRED.png"));
        	REDFactory = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/FactoryRED.png"));
        	REDHouse = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/HouseRED.png"));
        	REDLab = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/LabRED.png"));
        	REDSolar = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/SolarPanelsRED.png"));
        	REDRecycle = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/RecycleRED.png"));
        	REDNuclear = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/NuclearRED.png"));
        	REDLandfill = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/LandfillRED.png"));
        	REDWaterCleaner = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/WaterCleanerRED.png"));
        	REDTree = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/Tree1RED.png"));
        	
        	GREENBarn = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/BarnGREEN.png"));
        	GREENFactory = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/FactoryGREEN.png"));
        	GREENHouse = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/HouseGREEN.png"));
        	GREENLab = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/LabGREEN.png"));
        	GREENSolar = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/SolarPanelsGREEN.png"));
        	GREENRecycle = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/RecycleGREEN.png"));
        	GREENNuclear = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/NuclearGREEN.png"));
        	GREENLandfill = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/LandfillGREEN.png"));
        	GREENWaterCleaner = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/WaterCleanerGREEN.png"));
        	GREENTree = ImageIO.read(getClass().getResource("/project/snowflake/mousedImage/Tree1GREEN.png"));
        	
        	//buildingPlaceSound = getClass().getResourceAsStream("/project/snowflake/sounds/BuildingPlaceSound.wav");
        	//buildingDestroySound = getClass().getResourceAsStream("/project/snowflake/sounds/gameboom2.wav");
        	//buildingBoopSound = getClass().getResourceAsStream("/project/snowflake/sounds/gameplace.wav");
        } catch(Exception e) {
        	
        }
        
        //chunks.add(new Chunk(0,0));
        //Only this chunk because I'm stretching it to 
        //take up the space of three
        chunks.add(new BiomeChunk(-100, 0));
        //chunks.add(new Chunk(100, 0));
        //chunks.add(new DeepChunk(0, 100));
        //chunks.add(new DeepChunk(-100, 100));
        //chunks.add(new DeepChunk(100, 100));
        //waveform = 60000 * Math.pow(Math.sin((900000/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2);
        int biomeType = ((BiomeChunk)chunks.get(0)).index/20;
        if(biomeType == 0) {
        	 lastSwitch = System.currentTimeMillis();
        	 //dryDuration = (long)(Math.random() * 5000) + 60000;
        	 dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
        	 rainDuration = (long)(Math.random() * 3000) + 10000;
        	 nextRain = lastSwitch + dryDuration;
        	 nextDry = nextRain + rainDuration;
        }
        else if(biomeType == 1) {
        	lastSwitch = System.currentTimeMillis();
        	//dryDuration = (long)(Math.random() * 5000) + 60000;
        	dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
        	rainDuration = (long)(Math.random() * 3000) + 20000;
        	nextRain = lastSwitch + dryDuration;
       	 	nextDry = nextRain + rainDuration;
        }
        else if(biomeType == 2) {
        	lastSwitch = System.currentTimeMillis();
        	//dryDuration = (long)(Math.random() * 5000) + 60000;
        	dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
        	rainDuration = (long)(Math.random() * 3000) + 40000;
        	nextRain = lastSwitch + dryDuration;
        	nextDry = nextRain + rainDuration;
        }
        else if(biomeType == 3) {
        	lastSwitch = System.currentTimeMillis();
        	//dryDuration = (long)(Math.random() * 5000) + 60000;
        	dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
        	rainDuration = (long)(Math.random() * 3000) + 10000;
        	nextRain = lastSwitch + dryDuration;
        	nextDry = nextRain + rainDuration;
        }
        else {
        	lastSwitch = System.currentTimeMillis();
        	//dryDuration = (long)(Math.random() * 5000) + 10000;
        	dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
        	rainDuration = (long)(Math.random() * 3000) + 4000;
        	nextRain = lastSwitch + dryDuration;
        	nextDry = nextRain + rainDuration;
        }
        //lastSwitch = System.currentTimeMillis();
        //dryDuration = (long)(Math.random() * 500) + 1000;
        //dryDuration = (long) (60000 * Math.pow(Math.sin((900000/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
    	//rainDuration = (long)(Math.random() * 300) + 4000;
    	//nextRain = lastSwitch + dryDuration;
    	//nextDry = nextRain + rainDuration;
        //test001 = new AquiferStartingPoint(chunks.get(1));
        
    	numCoal = chunks.get(0).numCoal;
    	numWat = chunks.get(0).numWat;
    	cLog += numCoal + ":" + timeLine + "x";
    	aLog += numWat + ":" + timeLine + "x";
    	
        (new Thread(ticker)).start();
    }

    public int getRealX(int coordX) {
    	return (int) ((coordX + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2;
    }
    
    public int getRealY(int coordY) {
    	return (int) ((coordY + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2;
    }
    
    public void updateCO2Thingies() {
    	if(co2Thingies.size() > 0) {
    		int yolo = co2Thingies.size();
    		long dif = System.currentTimeMillis() - lastCO2ThingyUpdate;
    		for(int i = yolo - 1; i >= 0; i--) {
    			co2Thingies.get(i).y -= ((1000/(System.currentTimeMillis() - co2Thingies.get(i).birthday + 1)) * (((double)dif)/1000.0));
    			co2Thingies.get(i).x += 5 * (((double)dif)/1000.0);
    			if(System.currentTimeMillis() - co2Thingies.get(i).birthday > 2500) {
    				//System.out.println(co2Thingies.get(i).x);
    				co2Thingies.remove(i);
    			}
   			}
    	}
    	lastCO2ThingyUpdate = System.currentTimeMillis();
    }
    
    public void updateBackwardsCO2Thingies() {
    	if(backwards.size() > 0) {
    		int yolo = backwards.size();
    		long dif = System.currentTimeMillis() - lastBackwardsUpdate;
    		for(int i = yolo - 1; i >= 0; i--) {
    			backwards.get(i).y += ((2000/(System.currentTimeMillis() - backwards.get(i).birthday + 1)) * (((double)dif)/1000.0));
    			//backwards.get(i).y += 1;
    			//backwards.get(i).x -= 5 * (((double)dif)/1000.0);
    			if(System.currentTimeMillis() - backwards.get(i).birthday > 2500) {
    				//System.out.println(co2Thingies.get(i).x);
    				backwards.remove(i);
    			}
   			}
    	}
    	lastBackwardsUpdate = System.currentTimeMillis();
    }
    
    public void updateMunnyThings() {
    	if(munnythings.size() > 0) {
    		int yolo = munnythings.size();
    		long dif = System.currentTimeMillis() - lastMunnyThingUpdate;
    		for(int i = yolo - 1; i >= 0; i--) {
    			munnythings.get(i).y -= (((1000)/(System.currentTimeMillis() - munnythings.get(i).birthday + 1)) * (((double)dif)/1000.0));
    			//backwards.get(i).y += 1;
    			//backwards.get(i).x -= 5 * (((double)dif)/1000.0);
    			if(System.currentTimeMillis() - munnythings.get(i).birthday > 1000) {
    				//System.out.println(co2Thingies.get(i).x);
    				munnythings.remove(i);
    			}
   			}
    	}
    	lastMunnyThingUpdate = System.currentTimeMillis();
    }
    
    public void updateFaces() {
    	if(faces.size() > 0) {
    		int yolo = faces.size();
    		long dif = System.currentTimeMillis() - lastFaceUpdate;
    		for(int i = yolo - 1; i >= 0; i--) {
    			faces.get(i).y -= ((1000/(System.currentTimeMillis() - faces.get(i).birthday + 1)) * (((double)dif)/1000.0));
    			if(System.currentTimeMillis() - faces.get(i).birthday > 2500)
    				faces.remove(i);
    		}
    	}
    	lastFaceUpdate = System.currentTimeMillis();
    }
    public void updateElectrics() {
    	if(electrics.size() > 0) {
    		int yolo = electrics.size();
    		long dif = System.currentTimeMillis() - lastElectricUpdate;
    		for(int i = yolo - 1; i >= 0; i--) {
    			electrics.get(i).y -= ((1000/(System.currentTimeMillis() - electrics.get(i).birthday + 1)) * (((double)dif)/1000.0));
    			if(System.currentTimeMillis() - electrics.get(i).birthday > 500)
    				electrics.remove(i);
    		}
    	}
    	lastElectricUpdate = System.currentTimeMillis();
    }
    
    public synchronized void start() {
        isRunning = true;
        new Thread(this).start();
    }

    public synchronized void stop() {
        isRunning = false;
    }

    public void run() {
        while (isRunning) {
            //System.out.println("Hej");
        	if(state == GameState.GAME) {
        		tick();
            	render();
            	
        	}
        	else if(state == GameState.MENU) {
        		tickMain();
        		renderMain();
        	}
        	/*long dif = System.currentTimeMillis() - lastCloudMove;
        	for(int i = 0; i < clouds.size(); i++) {
        		clouds.get(i).x += 10*(((double) dif)/1000);
        		if(clouds.get(i).x > WIDTH) {
        			clouds.get(i).x = -200;
        		}
        	}
        	lastCloudMove = System.currentTimeMillis();*/
            /*try {
                Thread.sleep(0);
            } catch (Exception e) {
            }*/
        }
    }
    
    public void tickMain() {
    	
    }
    
    public void renderMain() {
    	BufferStrategy bs = getBufferStrategy();
        if(bs == null) {
            createBufferStrategy(3);
            return;
        }
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        
        g.setColor(backColor);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        
        for(int i = 0; i < clouds.size(); i++) {
        	if(clouds.get(i) instanceof StorCloud) {
        		g.drawImage(BigCloud, (int) clouds.get(i).x, (int) clouds.get(i).y, null);
        	}
        	else {
        		g.drawImage(SmallCloud, (int) clouds.get(i).x, (int) clouds.get(i).y, null);
        	}
        }
        
        g.drawImage(playButton, WIDTH/2 - 100, HEIGHT/2, null);
        g.drawImage(tutorialButton, WIDTH/2 - 275, HEIGHT/2 + 100, null);
        g.drawImage(titleDisplay, WIDTH/2 - 500, 0, null);
        
    	g.dispose();
        bs.show();
    }
    
    public void tick() {
    	if(System.currentTimeMillis() - lastSave > 60000) {
    		if(!fileSave.equals("")) {
    			save();
    		}
    		lastSave = System.currentTimeMillis();
    	}
    	
    	if(raining) {
    		if(System.currentTimeMillis() - lastSwitch > rainDuration) {
    			//dryDuration = (long) (6000 * Math.pow(Math.sin((900000/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2)) + 15000;
    			dryDuration = (long) (amplitude * Math.pow(Math.sin((period/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
    			//dryDuration = 6000;
    			raining = false;
    			lastSwitch = System.currentTimeMillis();
    			wLog += "0:"+ timeLine + "x";
    			for(int i = 0; i < buildings.size(); i++) {
    				if(buildings.get(i) instanceof House) {
    					((House)buildings.get(i)).mineIndex = 0;
    				}
    				if(buildings.get(i) instanceof Mine) {
    					((Mine)buildings.get(i)).mineIndex = 0;
    				}
    				if(buildings.get(i) instanceof Nuclear) {
    					((Nuclear)buildings.get(i)).mineIndex = 0;
    				}
    			}
    		}
    	}
    	else {
    		if(System.currentTimeMillis() - lastSwitch > dryDuration) {
    			//dryDuration = (long) (60000 * Math.pow(Math.sin((900000/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2));
    			raining = true;
    			lastSwitch = System.currentTimeMillis();
    			rainx = -1000;
    			rainy = -600;
    			wLog += "1:" + timeLine + "x";
    		}
    	}
    	
    	//FPS
    	//System.out.println(1000/(System.currentTimeMillis() - lastUpdate));
    	long dt = System.currentTimeMillis() - lastUpdate;
    	timeLine += dt;
    	lastUpdate = System.currentTimeMillis();
    	font4 = new Font("Comic Sans MS", Font.BOLD, (int) (35*HEIGHTSCALE));
    	if(!doneBefore) {
    		boolean dont = false;
    		for(int i = 0; i < chunks.get(0).blocks.length - 5; i++) {
    			for(int z = 0; z < buildings.size(); z++) {
    				if(buildings.get(z).x + 5 > i){
    					dont = true;
    				}
    			}
    			if(!dont) {
    				if(Math.random() >= 0.8) {
    					int biomeType = ((BiomeChunk)chunks.get(0)).index/20;
    					Tree tree = new Tree(0);
    					if(biomeType == 0) {
    						tree = new Tree(5);
    					}
    					else if(biomeType == 1) {
    						tree = new Tree(7);
    					}
    					else if(biomeType == 2) {
    						if(Math.random() < 0.5) {
    							tree = new Tree(4);
    						}
    						else {
    							tree = new Tree(3);
    						}
    					}
    					else if(biomeType == 3) {
    						if(Math.random() < 0.5) {
    							tree = new Tree(7);
    						}
    						else {
    							tree = new Tree(6);
    						}
    					}
    					else if(biomeType == 4) {
    						tree = new Tree(3);
    					}
    					else {
    						tree = new Tree(1);
    					}
    	    			tree.x = i;
    	    			tree.y = chunks.get(0).dirtLevel - 4;
    	    			buildings.add(tree);
    				}
    			}
    			dont = false;
    		}
    		doneBefore = true;
    	}
    	if(System.currentTimeMillis() - lastConsumptionUpdate >= 1000) {
    		calculatePerCapitas();
    		calcNewFoodings();
    		calcNewEnergies();
    		calcHappiness();
    		lastConsumptionUpdate = System.currentTimeMillis();
    	}
    	
    	updateCO2Thingies();
    	updateBackwardsCO2Thingies();
    	updateFaces();
    	updateMunnyThings();
    	updateElectrics();
    	
    	if(System.currentTimeMillis() - lastClean >= 10 && powerPlantCount == 0) {
    		if(pollution >= 0.007) {
            	pollution -= 0.007;
            }
    		lastClean = System.currentTimeMillis();
    	}
        if(rightHeld && getRealX(chunks.get(0).blocks.length - 25) > 0) {
        	for(int j = 0; j < chunks.size(); j++) {
        		chunks.get(j).x -= movementSpeed;
        		if(mousedBuilding != null) {
        			mousedBuilding.x += movementSpeed;
        			mousedBuildingCol = false;
        			mousedBuilding.x = (int) (((mouseX - WIDTH/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).x));
        			if(mousedBuilding.x < 0) {
        				mousedBuilding.x = 0;
        			}
        			if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
        				if(mousedBuilding.x > 165) {
        					mousedBuilding.x = 165;
        				}
        			}
        			else {
        				if(mousedBuilding.x > 170) {
        					mousedBuilding.x = 170;
        				}
        			}
    				for(int i = 0; i < buildings.size(); i++) {
    					if(buildings.get(i) instanceof PowerPlant || buildings.get(i) instanceof Mine || buildings.get(i) instanceof Nuclear) {
    						if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
    							if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 10 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    						else {
    							if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 5 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    					}
    					else {
    						if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
    							if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 10 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    						else {
    							if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 5 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    					}
    				}
        		}
        	}
        }
        if(leftHeld && getRealX(25) < WIDTH) {
        	for(int j = 0; j < chunks.size(); j++) {
        		chunks.get(j).x += movementSpeed;
        		if(mousedBuilding != null) {
        			mousedBuilding.x -= movementSpeed;
        			mousedBuildingCol = false;
        			mousedBuilding.x = (int) (((mouseX - WIDTH/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).x));
        			if(mousedBuilding.x < 0) {
        				mousedBuilding.x = 0;
        			}
        			if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
        				if(mousedBuilding.x > 165) {
        					mousedBuilding.x = 165;
        				}
        			}
        			else {
        				if(mousedBuilding.x > 170) {
        					mousedBuilding.x = 170;
        				}
        			}
    				for(int i = 0; i < buildings.size(); i++) {
    					if(buildings.get(i) instanceof PowerPlant || buildings.get(i) instanceof Mine || buildings.get(i) instanceof Nuclear) {
    						if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
    							if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 10 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    						else {
    							if(mousedBuilding.x < buildings.get(i).x + 10 && mousedBuilding.x + 5 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    					}
    					else {
    						if(mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Mine || mousedBuilding instanceof Nuclear) {
    							if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 10 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    						else {
    							if(mousedBuilding.x < buildings.get(i).x + 5 && mousedBuilding.x + 5 > buildings.get(i).x) {
    								mousedBuildingCol = true;
    								return;
    							}
    						}
    					}
    				}
        		}
        	}
        }
        if(upHeld && getRealY(chunks.get(0).dirtLevel + 15) < HEIGHT) {
        	for(int i = 0; i < chunks.size(); i++) {
        		chunks.get(i).y += movementSpeed;
        		if(mousedBuilding != null);
        			//mousedBuilding.y -= movementSpeed;
        	}
        }
        if(downHeld && getRealY(85) > 0) {
        	for(int i = 0; i < chunks.size(); i++) {
        		chunks.get(i).y -= movementSpeed;
        		if(mousedBuilding != null);
        			//mousedBuilding.y += movementSpeed;
        	}
        }
        if(zoomingIn) {
        	if(THEOHEIGHTSCALE < MAXZOOM)
        		THEOHEIGHTSCALE += 0.02;
        }
        if(zoomingOut) {
        	if(THEOHEIGHTSCALE > MINZOOM)
        		THEOHEIGHTSCALE -= 0.02;
        }
        
        int yoloVehicleSize = inGames.size() - 1;
        for(int i = yoloVehicleSize; i >= 0; i--) {
        	long dif = System.currentTimeMillis() - inGames.get(i).lastMove;
        	if(inGames.get(i).goingLeft) {
        		inGames.get(i).x -= 10 * ((double)dif/1000);
        		inGames.get(i).lastMove = System.currentTimeMillis();
        		if(inGames.get(i).x <= inGames.get(i).destinationx) {
        			inGames.remove(i);
        		}
        	}
        	else {
        		inGames.get(i).x += 10 * ((double)dif/1000);
        		inGames.get(i).lastMove = System.currentTimeMillis();
        		if(inGames.get(i).x >= inGames.get(i).destinationx) {
        			inGames.remove(i);
        		}
        	}
        }
        
        for(int i = 0; i < buildings.size(); i++) {
        	if(buildings.get(i) instanceof Tree) {
        		if(System.currentTimeMillis() - ((Tree)buildings.get(i)).lastSequestration > ((Tree)buildings.get(i)).sequestrationTime) {
        			if(pollution >= 20) {
        				backwards.add(new CO2Thingy(buildings.get(i).x, buildings.get(i).y - 4));
        			}
        			pollution = addInRange(0, 100, pollution, -0.2);
        			((Tree)buildings.get(i)).lastSequestration = System.currentTimeMillis();
        		}
        	}
        	if(buildings.get(i) instanceof Mine && ((Math.random()) < farmWaterAllowance/1000)) {
        		try {
        			boolean x = false;
        			if((System.currentTimeMillis() - ((Mine) buildings.get(i)).lastMine) >= 400 && waters < 1000) {
        				
        				//if(!((Mine) buildings.get(i)).wat) {
        					if(chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] instanceof AquiferBlock || chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] instanceof CoalBlock || chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] instanceof LimestoneBlock || chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] instanceof DirtBlock || chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] instanceof DirtyWaterBlock) {
        						Block placeHolder = chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex];
        						chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = null;
        						if(placeHolder instanceof AquiferBlock) {
        							chunks.get(0).blocks[((Mine)buildings.get(i)).x][((Mine)buildings.get(i)).y + 5 + ((Mine)buildings.get(i)).mineIndex] = new DarkStoneBlock();
            						//nucTurt.add(new WaterDirtyTurtle(((Mine)buildings.get(i)).x, ((Mine)buildings.get(i)).y + 5 + ((Mine)buildings.get(i)).mineIndex));
            						//((Mine)buildings.get(i)).lastPollute = System.currentTimeMillis();
            						((Mine)buildings.get(i)).wat = false;
            						numWat -= 1;
            						aLog += numWat + ":" + timeLine + "x";
            						vALog += waters + ":" + timeLine + "x";
        							waters += 1;
        							drinkingTurtles.add(new WaterTurtle(buildings.get(i).x, buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex));
        							//chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new AquiferBlock();
        							((Mine) buildings.get(i)).wat = true;
        						}
        						else if(placeHolder instanceof DirtyWaterBlock) {
        							chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new DirtyWaterBlock();
        						}
        						else if(placeHolder instanceof CoalBlock) {
        							//fossilFyools += 1;
        							//turtles.add(new MiningTurtle(buildings.get(i).x, buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex));
        							//chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new DiscoFFBlock();
        							chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new DarkStoneBlock();
        							numCoal -= 1;
        							cLog += numCoal + ":" + timeLine + "x";
        						}
        						else if(placeHolder instanceof LimestoneBlock || placeHolder instanceof DirtBlock || placeHolder instanceof DarkDirtBlock) {
        							chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new DarkDirtBlock();
        						}
        						else if(placeHolder instanceof DarkStoneBlock) {
        							x = true;
        						}
        					}
        					else {
        						chunks.get(0).blocks[buildings.get(i).x][buildings.get(i).y + 5 + ((Mine) buildings.get(i)).mineIndex] = new DarkStoneBlock();
        					}
        					if(!x) {
        						((Mine) buildings.get(i)).lastMine = System.currentTimeMillis();
        					} 
        					else {
        						((Mine)buildings.get(i)).lastMine += 1000;
        					}
        					((Mine) buildings.get(i)).mineIndex += 1;
        				//}
        			}
        		} catch(Exception e) {System.out.println("poop1");}
        		if(System.currentTimeMillis() - ((Mine)buildings.get(i)).lastCrop >= 1000 && waters > 9) {
        			System.out.println("farmed");
        			((Mine)buildings.get(i)).lastCrop = System.currentTimeMillis();
        			foodings += 100 + (10 * (scienceLevel-1));;
        			waters -= 5;
        			vALog += waters + ":" + timeLine + "x";
        			vALog += waters + ":" + timeLine + "x";
        			/*if(((Mine)buildings.get(i)).wat) {
        				foodings += 30 + (10 * (scienceLevel-1));
        			}*/
        			munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+Food"));
        		}
        	}
        	else if(buildings.get(i) instanceof SolarPlant) {
        		pricePerKWH = 0.1/(Math.abs(energyPerCapita)+5);
        		if(System.currentTimeMillis() - ((SolarPlant)buildings.get(i)).lastTake >= 1000) {
        			int energyDeriv = 10 + (10*(scienceLevel-1));
        			energies += energyDeriv + scienceLevel;
        			munnies += (energyDeriv + scienceLevel) * pricePerKWH;
        			//munnies += (long) (energyDeriv * Math.pow(Math.sin((900000/(2*Math.PI)) * (System.currentTimeMillis()-startTime)), 2)) + 15000;
        			electrics.add(new plusElectric(buildings.get(i).x, buildings.get(i).y + 1));
        			if(energyDeriv * pricePerKWH < 0.01) {
        				munnies += 0.01;
        				munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+$0.01"));
        			}
        			else {
        				munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+$" + df.format(energyDeriv * pricePerKWH)));
        			}
        			//playBoopSound();
        			((SolarPlant)buildings.get(i)).lastTake = System.currentTimeMillis();
        		}
        	}
        	else if(buildings.get(i) instanceof Nuclear) {
        		if(System.currentTimeMillis() - ((Nuclear)buildings.get(i)).lastTick >= 1000 && ((Nuclear)buildings.get(i)).watFound) {
        			energies += 60;
        			electrics.add(new plusElectric(buildings.get(i).x, buildings.get(i).y - 5));
        			((Nuclear)buildings.get(i)).lastTick = System.currentTimeMillis();
        		}
        		try {
        			if(System.currentTimeMillis() - ((Nuclear)buildings.get(i)).lastMine >= 1000) {
        				if(chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof LimestoneBlock || chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof DirtBlock || chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof DarkDirtBlock) {
    						chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] = new DarkDirtBlock();
    						//((Nuclear)buildings.get(i)).watFound = false;
    					}
        				/*else if(chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					
        				}*/
        				else if(chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] = new DirtyWaterBlock();
        					((Nuclear)buildings.get(i)).watFound = true;
        					nucTurt.add(new WaterDirtyTurtle(((Nuclear)buildings.get(i)).x, ((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex));
        				}
        				else if(chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof CoalBlock) {
        					chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] = new DiscoFFBlock();
        					numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
        				}
    					else {
    						chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] = new DiscoFFBlock();
    						//((Nuclear)buildings.get(i)).watFound = false;
    					}
    					((Nuclear)buildings.get(i)).mineIndex += 1;
    					((Nuclear)buildings.get(i)).lastMine = System.currentTimeMillis();
        			}
        		} catch(Exception e) {System.out.println("poop2");}
        	}
        	else if(buildings.get(i) instanceof Landfill) {
        		if(System.currentTimeMillis() - ((Landfill)buildings.get(i)).lastTick >= 1000 && ((Landfill)buildings.get(i)).watFound) {
        			pollution = addInRange(0, 100, pollution, 0.005*population);
        			((Landfill)buildings.get(i)).lastTick = System.currentTimeMillis();
        		}
        		try {
        			if(System.currentTimeMillis() - ((Landfill)buildings.get(i)).lastMine >= 1000) {
        				if(chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] instanceof LimestoneBlock || chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] instanceof DirtBlock || chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] instanceof DarkDirtBlock) {
    						chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] = new DarkDirtBlock();
    						((Landfill)buildings.get(i)).watFound = false;
    					}
        				/*else if(chunks.get(0).blocks[((Nuclear)buildings.get(i)).x][((Nuclear)buildings.get(i)).y + 5 + ((Nuclear)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					
        				}*/
        				else if(chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] = new DirtyWaterBlock();
        					((Landfill)buildings.get(i)).watFound = true;
        					nucTurt.add(new WaterDirtyTurtle(((Landfill)buildings.get(i)).x, ((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex));
        				}
        				else if(chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] instanceof CoalBlock) {
        					chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] = new DarkStoneBlock();
        					numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
        				}
    					else {
    						chunks.get(0).blocks[((Landfill)buildings.get(i)).x][((Landfill)buildings.get(i)).y + 5 + ((Landfill)buildings.get(i)).mineIndex] = new DarkStoneBlock();
    						((Landfill)buildings.get(i)).watFound = false;
    					}
    					((Landfill)buildings.get(i)).mineIndex += 1;
    					((Landfill)buildings.get(i)).lastMine = System.currentTimeMillis();
        			}
        		} catch(Exception e) {System.out.println("poop3");}
        	}
        	else if(buildings.get(i) instanceof RecyclePlant) {
        		if(System.currentTimeMillis() - ((RecyclePlant)buildings.get(i)).lastTake >= 1000 && energies >= 3) {
        			energies -= 3;
        			double possibleCleanFactor = 0.5 + (0.2*(scienceLevel-1));
        			if(pollution > possibleCleanFactor) {
        				pollution -= possibleCleanFactor;
        			}
        			((RecyclePlant)buildings.get(i)).lastTake = System.currentTimeMillis();
        		}
        	}
        	else if(buildings.get(i) instanceof PowerPlant) {
        		if(System.currentTimeMillis() - ((PowerPlant)buildings.get(i)).lastTake >= 1000 && fossilFyools >= 2 && waters > 0) {
        			energies += 30 + (3*(scienceLevel-1));
        			if(waters >= 1) {
            			waters -= 1;
            			vALog += waters + ":" + timeLine + "x";
            		}
        			fossilFyools -= 2;
        			co2Thingies.add(new CO2Thingy(buildings.get(i).x, buildings.get(i).y - 5));
        			electrics.add(new plusElectric(buildings.get(i).x, buildings.get(i).y - 3));
        			pollution = addInRange(0, 100, pollution, 1 + airCleanFactor);
        			((PowerPlant)buildings.get(i)).lastTake = System.currentTimeMillis();
        		}
        		try {
        			if(System.currentTimeMillis() - ((PowerPlant)buildings.get(i)).lastMine >= 100 && fossilFyools < ffreq) {
        				Block placeHolder = chunks.get(0).blocks[((PowerPlant)buildings.get(i)).x][((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex];
        				chunks.get(0).blocks[((PowerPlant)buildings.get(i)).x][((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex] = new DiscoFFBlock();
        				if(placeHolder instanceof AquiferBlock) {
        					watTurt.add(new WaterDirtyTurtle(((PowerPlant)buildings.get(i)).x, ((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex));
        					chunks.get(0).blocks[((PowerPlant)buildings.get(i)).x][((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex] = new DirtyWaterBlock();;
        				}
        				else if(placeHolder instanceof CoalBlock) {
        					turtles.add(new MiningTurtle(((PowerPlant)buildings.get(i)).x, ((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex));
        					chunks.get(0).blocks[((PowerPlant)buildings.get(i)).x][((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex] = new DiscoFFBlock();
        					fossilFyools += 5;
        					numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
        				}
        				else if(placeHolder instanceof LimestoneBlock || placeHolder instanceof DirtBlock || placeHolder instanceof DarkDirtBlock) {
        					chunks.get(0).blocks[((PowerPlant)buildings.get(i)).x][((PowerPlant)buildings.get(i)).y + 5 + ((PowerPlant)buildings.get(i)).mineIndex] = new DarkDirtBlock();
        				}
        				else {
        					
        				}
        				
        				((PowerPlant)buildings.get(i)).lastMine = System.currentTimeMillis();
        				((PowerPlant)buildings.get(i)).mineIndex += 1;
        			}
        		} catch(Exception e) {System.out.println("poop4");}
        	}
        	else if(buildings.get(i) instanceof House && ((Math.random()) < houseWaterAllowance/1000)) {
        		if(System.currentTimeMillis() - ((House)buildings.get(i)).lastTick >= 1000 && energies >= 1) {
        			//energies -= 1;
        			if(pollution >= 80) {
        				population -= 1;
        				if(population < 3) {
        					population = 3;
        				}
        			}
        			if(buildings.size() > 1 && Math.random() > 0.8) {
        				//int vehicleIndex = (int) (Math.random() * (buildings.size()));
        				int vehicleIndex = doThatShit(i);
        				if(((House)buildings.get(i)).transportationType == 0) {
        					inGames.add(new GasCar(buildings.get(i).x, buildings.get(i).y, buildings.get(vehicleIndex).x));
        					fossilFyools -= 3;
        					if(pollution < 98) {
        						pollution += 3;
        					}
        					co2Thingies.add(new CO2Thingy(buildings.get(i).x, buildings.get(i).y - 5));
        				}
        				else if(((House)buildings.get(i)).transportationType == 1 && energies >= 3) {
        					inGames.add(new ElectricCar(buildings.get(i).x, buildings.get(i).y, buildings.get(vehicleIndex).x));
        					energies -= 3;
        				}
        				else if(((House)buildings.get(i)).transportationType == 2) {
        					inGames.add(new Bus(buildings.get(i).x, buildings.get(i).y, buildings.get(vehicleIndex).x));
        					fossilFyools -= 1;
        					if(pollution < 100) {
        						pollution += 1;
        					}
        					co2Thingies.add(new CO2Thingy(buildings.get(i).x, buildings.get(i).y - 5));
        				}
        				else {
        					if(energies >= 1) {
        						inGames.add(new LightRail(buildings.get(i).x, buildings.get(i).y, buildings.get(vehicleIndex).x));
        						energies -= 1;
        					}
        				}
        			}
        			if(waters > 0) {
        				waters -= 1;
        				vALog += waters + ":" + timeLine + "x";
        				System.out.println("WATER TAKEN");
        			}
        			if(fossilFyools >= 1) {
        				fossilFyools -= 1;
        			}
        			if(foodings >= 1) {
        				//foodings -= 1;
        			}
        			if(((House)buildings.get(i)).watFound || waters >= 1) {
        				munnies += taxes/200;
        				munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+$" + taxes/200));
        			}
        			else {
        				munnies += taxes/400;
        				munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+$" + taxes/400));
        			}
        			
        			//playBoopSound();
        			((House)buildings.get(i)).lastTick = System.currentTimeMillis();
        		}
        		try {
        			if(System.currentTimeMillis() - ((House)buildings.get(i)).lastMine >= 10 && waters < waterLimit) {
        				if(chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					((House)buildings.get(i)).watFound = true;
        					//drinkingTurtles.add(new WaterTurtle(buildings.get(i).x, buildings.get(i).y + 5 + ((House) buildings.get(i)).mineIndex));
        					waters += 1;
        					vALog += waters + ":" + timeLine + "x";
        					if(recycleCount == 0 && System.currentTimeMillis() - ((House)buildings.get(i)).lastPollute >= 1000) {
        						chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] = new DarkStoneBlock();
        						drinkingTurtles.add(new WaterTurtle(((House)buildings.get(i)).x, ((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex));
        						((House)buildings.get(i)).lastPollute = System.currentTimeMillis();
        						numWat -= 1;
        						aLog += numWat + ":" + timeLine + "x";
        						((House)buildings.get(i)).watFound = false;
        					}
        				}
        				else if(chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof DirtyWaterBlock) {
        					((House)buildings.get(i)).mineIndex += 1;
        				}
        				else {
        					if(chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof LimestoneBlock || chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof DirtBlock || chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof DarkDirtBlock) {
        						chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] = new DarkDirtBlock();
        					}
        					else if(chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] instanceof CoalBlock) {
        						chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] = new DarkStoneBlock();
        						numCoal -= 1;
    							cLog += numCoal + ":" + timeLine + "x";
        					}
        					else {
        						chunks.get(0).blocks[((House)buildings.get(i)).x][((House)buildings.get(i)).y + 5 + ((House)buildings.get(i)).mineIndex] = new DarkStoneBlock();
        					}
        					((House)buildings.get(i)).mineIndex += 1;
        				}
        				((House)buildings.get(i)).lastMine = System.currentTimeMillis();
        			}
        		} catch(Exception e) {System.out.println("poop5");}
        	}
        	else if(buildings.get(i) instanceof WaterCleaner) {
        		((WaterCleaner)buildings.get(i)).lastTick = System.currentTimeMillis();
        		try {
        			if(System.currentTimeMillis() - ((WaterCleaner)buildings.get(i)).lastMine >= 100) {
        				if(chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof LimestoneBlock || chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof DirtBlock || chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof DarkDirtBlock) {
    						chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] = new DarkDirtBlock();
    					}
        				else if(chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof AquiferBlock) {
        					
        				}
        				else if(chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof DirtyWaterBlock) {
        					chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] = new AquiferBlock();
        					cleanTurt.add(new WaterCleanTurtle(((WaterCleaner)buildings.get(i)).x, ((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex));
        				}
        				else if(chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] instanceof CoalBlock) {
        					chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] = new DarkStoneBlock();
        					numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
        				}
    					else {
    						chunks.get(0).blocks[((WaterCleaner)buildings.get(i)).x][((WaterCleaner)buildings.get(i)).y + 5 + ((WaterCleaner)buildings.get(i)).mineIndex] = new DarkStoneBlock();
    					}
    					((WaterCleaner)buildings.get(i)).mineIndex += 1;
    					((WaterCleaner)buildings.get(i)).lastMine = System.currentTimeMillis();
        			}
        		} catch(Exception e) {System.out.println("poop6");}
        	}
        	else if(buildings.get(i) instanceof ScienceFacility) {
        		if(System.currentTimeMillis() - ((ScienceFacility)buildings.get(i)).lastScienceUpgrade >= 1000 && munnies >= 1 && energies >= 1) {
        			energies -= 1;
        			munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y - 1, "-$1"));
        			munnythings.add(new MunnyParticle(buildings.get(i).x, buildings.get(i).y, "+Science"));
        			munnies -= 1;
        			science += 1;
        			if(science >= gradReq + (scienceLevel - 1)*20) {
        				scienceLevel += 1;
        				gradReq = scienceLevel*20;
        				if(scienceLevel == 2) {
        					try {
        						IconHolder.solarIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/SolarIcon.png"));
        					} catch(Exception e) {}
        				}
        				if(scienceLevel == 5) {
        					try {
        						IconHolder.recycleIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Recycle.png"));
        					} catch(Exception e) {}
        				}
        				if(scienceLevel == 15) {
        					try {
        						IconHolder.nuclearIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/NuclearIcon.png"));
        					} catch(Exception e) {}
        				}
        				if(scienceLevel == 10) {
        					try {
        						IconHolder.cleanerIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/WaterCleaner.png"));
        					} catch(Exception e) {}
        				}
        				if(scienceLevel == 25) {
        					try {
        						IconHolder.treeIcon = ImageIO.read(getClass().getResource("/project/snowflake/images/Tree1.png"));
        					} catch(Exception e) {}
        				}
        			}
        			((ScienceFacility)buildings.get(i)).lastScienceUpgrade = System.currentTimeMillis();
        		}
        	}
        }
        for(int i = 0; i < turtles.size(); i++) {
        	if(System.currentTimeMillis() - turtles.get(i).lastMine >= 100 && waters < waterLimit) {
        		try {
        			if(chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] instanceof AquiferBlock || chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] instanceof CoalBlock && fossilFyools < ffreq + turtles.size() - i) {
        				if(chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] instanceof AquiferBlock) {
    						waters += 1;
    						vALog += waters + ":" + timeLine + "x";
    					}
    					if(chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] instanceof CoalBlock) {
    						fossilFyools += 5;
    						//chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] = null;
    						chunks.get(0).blocks[turtles.get(i).x + turtles.get(i).rightIndex][turtles.get(i).y] = new DiscoFFBlock();
    						numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
    					}
    					
    					turtles.get(i).rightIndex += 1;
        			}
        			if(chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] instanceof AquiferBlock || chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] instanceof CoalBlock && fossilFyools < ffreq + turtles.size() - i) {
        				if(chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] instanceof AquiferBlock) {
    						waters += 1;
    						vALog += waters + ":" + timeLine + "x";
    					}
    					if(chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] instanceof CoalBlock) {
    						fossilFyools += 5;
    						//chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] = null;
    						chunks.get(0).blocks[turtles.get(i).x - turtles.get(i).leftIndex][turtles.get(i).y] = new DiscoFFBlock();
    						numCoal -= 1;
							cLog += numCoal + ":" + timeLine + "x";
    					}
    					
    					turtles.get(i).leftIndex += 1;
        			}
        			turtles.get(i).lastMine = System.currentTimeMillis();
        		} catch(Exception e) {System.out.println("poop7");}
        	}
        }
        for(int i = 0; i < watTurt.size(); i++) {
        	if(System.currentTimeMillis() - watTurt.get(i).lastMine >= 100) {
        		try {
        			if(chunks.get(0).blocks[watTurt.get(i).x + watTurt.get(i).rightIndex][watTurt.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[watTurt.get(i).x + watTurt.get(i).rightIndex][watTurt.get(i).y] = new DirtyWaterBlock();
        				watTurt.get(i).rightIndex += 1;
					}
        			if(chunks.get(0).blocks[watTurt.get(i).x - watTurt.get(i).leftIndex][watTurt.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[watTurt.get(i).x - watTurt.get(i).leftIndex][watTurt.get(i).y] = new DirtyWaterBlock();
        				watTurt.get(i).leftIndex += 1;
					}
        			if(chunks.get(0).blocks[watTurt.get(i).x + watTurt.get(i).rightIndex][watTurt.get(i).y] instanceof DarkStoneBlock) {
        				watTurt.get(i).rightIndex += 1;
					}
        			if(chunks.get(0).blocks[watTurt.get(i).x - watTurt.get(i).leftIndex][watTurt.get(i).y] instanceof DarkStoneBlock) {
        				watTurt.get(i).leftIndex += 1;
					}
        		} catch(Exception e) {System.out.println("poop8");}
        		watTurt.get(i).lastMine = System.currentTimeMillis();
        	}
        }
        for(int i = 0; i < nucTurt.size(); i++) {
        	if(System.currentTimeMillis() - nucTurt.get(i).lastMine >= 1000) {
        		try {
        			if(chunks.get(0).blocks[nucTurt.get(i).x + nucTurt.get(i).rightIndex][nucTurt.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[nucTurt.get(i).x + nucTurt.get(i).rightIndex][nucTurt.get(i).y] = new DirtyWaterBlock();
        				nucTurt.get(i).rightIndex += 1;
					}
        			if(chunks.get(0).blocks[nucTurt.get(i).x - nucTurt.get(i).leftIndex][nucTurt.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[nucTurt.get(i).x - nucTurt.get(i).leftIndex][nucTurt.get(i).y] = new DirtyWaterBlock();
        				nucTurt.get(i).leftIndex += 1;
					}
        		} catch(Exception e) {System.out.println("poop9");}
        		nucTurt.get(i).lastMine = System.currentTimeMillis();
        	}
        }
        for(int i = 0; i < cleanTurt.size(); i++) {
        	if(System.currentTimeMillis() - cleanTurt.get(i).lastMine >= 100) {
        		try {
        			if(chunks.get(0).blocks[cleanTurt.get(i).x + cleanTurt.get(i).rightIndex][cleanTurt.get(i).y] instanceof DirtyWaterBlock) {
        				chunks.get(0).blocks[cleanTurt.get(i).x + cleanTurt.get(i).rightIndex][cleanTurt.get(i).y] = new AquiferBlock();
        				cleanTurt.get(i).rightIndex += 1;
					}
        			if(chunks.get(0).blocks[cleanTurt.get(i).x - cleanTurt.get(i).leftIndex][cleanTurt.get(i).y] instanceof DirtyWaterBlock) {
        				chunks.get(0).blocks[cleanTurt.get(i).x - cleanTurt.get(i).leftIndex][cleanTurt.get(i).y] = new AquiferBlock();
        				cleanTurt.get(i).leftIndex += 1;
					}
        		} catch(Exception e) {System.out.println("poop10");}
        		cleanTurt.get(i).lastMine = System.currentTimeMillis();
        	}
        }
        for(int i = 0; i < drinkingTurtles.size(); i++) {
        	if(System.currentTimeMillis() - drinkingTurtles.get(i).lastMine >= 100 && waters < waterLimit) {
        		try {
        			if(chunks.get(0).blocks[drinkingTurtles.get(i).x + drinkingTurtles.get(i).rightIndex][drinkingTurtles.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[drinkingTurtles.get(i).x + drinkingTurtles.get(i).rightIndex][drinkingTurtles.get(i).y] = new DarkStoneBlock();
        				drinkingTurtles.get(i).rightIndex += 1;
        				waters += 1;
        				vALog += waters + ":" + timeLine + "x";
					}
        			if(chunks.get(0).blocks[drinkingTurtles.get(i).x - drinkingTurtles.get(i).leftIndex][drinkingTurtles.get(i).y] instanceof AquiferBlock) {
        				chunks.get(0).blocks[drinkingTurtles.get(i).x - drinkingTurtles.get(i).leftIndex][drinkingTurtles.get(i).y] = new DarkStoneBlock();
        				drinkingTurtles.get(i).leftIndex += 1;
        				waters += 1;
        				vALog += waters + ":" + timeLine + "x";
					}
        		} catch(Exception e) {System.out.println("poop11");}
        		drinkingTurtles.get(i).lastMine = System.currentTimeMillis();
        	}
        }
        
        if(System.currentTimeMillis() - lastWarningUpdate >= 1500) {
        	warnings.clear();
        	if(((double)popCap)/((double)population) <= 0.5) {
        		warnings.add("We want more houses!");
        	}
        	if(foodPerCapita <= 2) {
        		warnings.add("We want more food!");
        	}
        	if(energyPerCapita <= 2) {
        		warnings.add("We want more energy!");
        	}
        	if(pollution >= 50) {
        		warnings.add("We want less pollution!");
        	}
        	lastWarningUpdate = System.currentTimeMillis();
        }
    }
    
    public void render() {
       BufferStrategy bs = getBufferStrategy();
       if(bs == null) {
            createBufferStrategy(3);
            return;
        }
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        if(THEOHEIGHTSCALE != HEIGHTSCALE) {
        	HEIGHTSCALE = THEOHEIGHTSCALE;
        }
        g.fillRect(0,0,WIDTH, HEIGHT);
        g.setFont(font);
        g.clearRect(0, 0, WIDTH, HEIGHT);
        if(!raining) {
        	
        	Color fractionedColor = new Color(getColorFraction(backColor.getRed(), pollColor.getRed()), getColorFraction(backColor.getGreen(), pollColor.getGreen()), getColorFraction(backColor.getBlue(), pollColor.getBlue()));
        	//g.setColor(fractionedColor);
        	//currColor = fractionedColor;
        	double newRed = currColor.getRed();
        	double newGreen = currColor.getGreen();
        	double newBlue = currColor.getBlue();
        	newRed += (fractionedColor.getRed() - (double)currColor.getRed());
        	if(currColor.getGreen() != fractionedColor.getGreen()) {
        		newGreen += (fractionedColor.getGreen() - (double)currColor.getGreen())/20.0;
        	}
        	if(currColor.getBlue() != fractionedColor.getBlue()) {
        		newBlue += (fractionedColor.getBlue() - (double)currColor.getBlue())/20.0;
        	}
        	currColor = new Color((int)newRed, (int)newGreen, (int)newBlue);
        	//System.out.println(newRed + ", " + newGreen + ", " + newBlue);
        	//System.out.println(fractionedColor.getRed() + ", " + fractionedColor.getGreen() + ", " + fractionedColor.getBlue());
        }
        else {
        	double newRed = currColor.getRed();
        	double newGreen = currColor.getGreen();
        	double newBlue = currColor.getBlue();
        	if(currColor.getRed() != 64) {
        		newRed += (64 - (double)currColor.getRed());
        		//System.out.println((64 - (double)currColor.getRed())/20.0);
        	}
        	if(currColor.getGreen() != 64) {
        		newGreen += (64 - (double)currColor.getGreen())/20.0;
        		//System.out.println((64 - (double)currColor.getRed())/20.0);
        	}
        	if(currColor.getBlue() != 64) {
        		newBlue += (64 - (double)currColor.getBlue())/20.0;
        	}
        	currColor = new Color((int)newRed, (int)newGreen, (int)newBlue);
        	//System.out.println(newRed + ", " + newGreen + ", " + newBlue);
        	//g.setColor(Color.gray);
        }
        g.setColor(currColor);
        g.fillRect(0,0,WIDTH,HEIGHT);
        
        for(int i = 0; i < clouds.size(); i++) {
        	if(clouds.get(i) instanceof StorCloud) {
        		g.drawImage(BigCloud, (int) clouds.get(i).x, (int) clouds.get(i).y, null);
        	}
        	else {
        		g.drawImage(SmallCloud, (int) clouds.get(i).x, (int) clouds.get(i).y, null);
        	}
        }
        g.setColor(Color.gray);
        g.fillRect((int) ((chunks.get(0).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((chunks.get(0).y + chunks.get(0).dirtLevel + 1)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (chunks.get(0).blocks.length * 16 * HEIGHTSCALE) + 1, (int) ((chunks.get(0).blocks[0].length - chunks.get(0).dirtLevel) * 16 * HEIGHTSCALE) + 1);
        for(int i = 0; i < chunks.size(); i++) {
        	for(int k = 0; k < chunks.get(i).blocks[0].length; k++) {
        		for(int j = 0; j < chunks.get(i).blocks.length; j++) {
        			if(chunks.get(i).blocks[j][k] != null){
        				if(chunks.get(i).blocks[j][k] instanceof LimestoneBlock) {
        					if(chunks.get(i).blocks[j][k] instanceof SandBlock) {
        						g.drawImage(sand, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        					}
        					else if(chunks.get(i).blocks[j][k] instanceof PermaJordBlock) {
        						g.drawImage(permaJord, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        					}
        					else if(chunks.get(i).blocks[j][k] instanceof PermaGrassBlock) {
        						g.drawImage(permaGrass, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        					}
        					else {
        						g.drawImage(grass, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        					}
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DirtBlock) {
        					//g.setColor(new Color(128, 64, 0));
        					g.drawImage(dirt, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				/*else if(chunks.get(i).blocks[j][k] instanceof StoneBlock) {
        					//g.setColor(new Color(128,128,128));
        					g.drawImage(stone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}*/
        				else if(chunks.get(i).blocks[j][k] instanceof AquiferBlock && !(chunks.get(i).blocks[j][k] instanceof DiscoWaBlock)) {
        					//g.setColor(new Color(0, 0, 255));
        					g.drawImage(water, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DirtyWaterBlock) {
        					g.drawImage(dirtyWater, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof CoalBlock) {
        					g.drawImage(coal, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DiscoFFBlock) {
        					if(raining && Math.random() < 0.007) {
        						chunks.get(0).blocks[j][k] = new DirtyWaterBlock();
        					}
        					//g.setColor(new Color(255, 255, 64));
        					//g.fillRect((int) ((j + chunks.get(i).x)*16*HEIGHTSCALE) - 1 + WIDTH/2,(int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2,(int) (16*HEIGHTSCALE) + 2,(int) (16*HEIGHTSCALE) + 2);
        					g.drawImage(darkStone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DiscoWaBlock) {
        					g.setColor(new Color(0, 128, 0));
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DarkStoneBlock) {
        					if(raining && Math.random() < 0.007) {
        						chunks.get(0).blocks[j][k] = new AquiferBlock();
        						//waters += 1;
        						//numWat += 1;
        						//aLog += numWat + ":" + timeLine + "x";
        					}
        					g.drawImage(darkStone, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				else if(chunks.get(i).blocks[j][k] instanceof DarkDirtBlock) {
        					g.drawImage(darkDirt, (int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2, (int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2, (int) (16 * HEIGHTSCALE) + 1, (int) (16 * HEIGHTSCALE) + 1, null);
        				}
        				//g.fillRect((int) ((j + chunks.get(i).x)*16*HEIGHTSCALE) - 1 + WIDTH/2,(int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2,(int) (16*HEIGHTSCALE) + 2,(int) (16*HEIGHTSCALE) + 2);
        			}
        			else {
        				//g.setColor(Color.blue);
        				//g.fillRect((int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2,(int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2,(int) (16*HEIGHTSCALE) + 2,(int) (16*HEIGHTSCALE) + 2);
        			}
        			//THIS IS THE SQUARE DRAW METHOD
        			//g.fillRect((int) ((j + chunks.get(i).x)*16*HEIGHTSCALE)- 1 + WIDTH/2,(int) ((k + chunks.get(i).y)*16*HEIGHTSCALE) - 1 + HEIGHT/2,(int) (16*HEIGHTSCALE) + 2,(int) (16*HEIGHTSCALE) + 2);
        			//g.fillRect((int) ((j + chunks.get(i).x)*16)- 1 + WIDTH/2,(int) ((k + chunks.get(i).y)*16) - 1 + HEIGHT/2,(int) (16) + 2,(int) (16) + 2);
        		}
        	}
        }
        for(int i = 0; i < buildings.size(); i++) {
        	if(buildings.get(i) instanceof House) {
        		g.drawImage(house, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof Mine) {
        		g.drawImage(barn, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (162 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof PowerPlant) {
        		g.drawImage(factory, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof ScienceFacility) {
        		g.drawImage(lab, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof SolarPlant) {
        		g.drawImage(solar, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof RecyclePlant) {
        		g.drawImage(recycle, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof WaterCleaner) {
        		g.drawImage(waterCleaner, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof Nuclear) {
        		g.drawImage(nuclear, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof Landfill) {
        		g.drawImage(landfill, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else if(buildings.get(i) instanceof Tree) {
        		if(((Tree)buildings.get(i)).treeType == 1) {
        			g.drawImage(Tree1, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 2) {
        			g.drawImage(Tree2, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 3) {
        			g.drawImage(Tree3, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 4) {
        			g.drawImage(Tree4, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 5) {
        			g.drawImage(Tree5, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 6) {
        			g.drawImage(Tree6, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else if(((Tree)buildings.get(i)).treeType == 7) {
        			g.drawImage(Tree7, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else {
        			g.drawImage(Tree1, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		//g.drawImage(Tree1, (int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 + (int) (10 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        	}
        	else {
        		g.fillRect((int) ((buildings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((buildings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE));
        	}
        }
        for(int i = 0; i < inGames.size(); i++) {
        	if(inGames.get(i) instanceof GasCar) {
        		if(inGames.get(i).goingLeft) {
        			g.drawImage(gasCarLEFT, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else {
        			g.drawImage(gasCar, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        	}
        	else if(inGames.get(i) instanceof ElectricCar) {
        		if(inGames.get(i).goingLeft) {
        			g.drawImage(electricCarLEFT, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        		else {
        			g.drawImage(electricCar, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (82 * HEIGHTSCALE), (int) (82 * HEIGHTSCALE), null);
        		}
        	}
        	else if(inGames.get(i) instanceof Bus) {
        		if(inGames.get(i).goingLeft) {
        			g.drawImage(bus, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + 3 + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (162/2 * HEIGHTSCALE), (int) (82/2 * HEIGHTSCALE), null);
        		}
        		else {
        			g.drawImage(busRIGHT, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + 3 + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (162/2 * HEIGHTSCALE), (int) (82/2 * HEIGHTSCALE), null);
        		}
        	}
        	else {
        		if(inGames.get(i).goingLeft) {
        			g.drawImage(lightRail, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + 3 + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (242/2 * HEIGHTSCALE), (int) (82/2 * HEIGHTSCALE), null);
        		}
        		else {
        			g.drawImage(lightRailRIGHT, (int) ((inGames.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((inGames.get(i).y + 3 + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (242/2 * HEIGHTSCALE), (int) (82/2 * HEIGHTSCALE), null);
        		}
        	}
        }
        
        //Post-chunk-drawing-loop
        if(mousedBuilding != null && !mousedBuildingCol) {
        	if(mousedBuilding instanceof House) {
        		g.drawImage(GREENHouse, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Mine) {
        		g.drawImage(GREENBarn, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (162 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof PowerPlant) {
        		g.drawImage(GREENFactory, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof ScienceFacility) {
        		g.drawImage(GREENLab, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof SolarPlant) {
        		g.drawImage(GREENSolar, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof RecyclePlant) {
        		g.drawImage(GREENRecycle, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof WaterCleaner) {
        		g.drawImage(GREENWaterCleaner, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Nuclear) {
        		g.drawImage(GREENNuclear, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Landfill) {
        		g.drawImage(GREENLandfill, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Tree) {
        		g.drawImage(GREENTree, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else {
        		g.setColor(Color.GREEN);
        		g.fillRect((int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE));
        		//g.fillRect((int) ((mousedBuilding.x + 2*chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + 2*chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE));
        		//g.fillRect(mousedBuilding.x, mousedBuilding.y, 100, 100);
        	}
        	if((mousedBuilding instanceof House || mousedBuilding instanceof Mine || mousedBuilding instanceof PowerPlant || mousedBuilding instanceof WaterCleaner || mousedBuilding instanceof Nuclear)) {
            	g.setColor(Color.GREEN);
            	g.fillRect((int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y + 5) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (16 * HEIGHTSCALE), (int) (100 * 16 * HEIGHTSCALE));
            }
        }
        else if(mousedBuilding != null && mousedBuildingCol) {
        	if(mousedBuilding instanceof House) {
        		g.drawImage(REDHouse, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Mine) {
        		g.drawImage(REDBarn, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (162 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof PowerPlant) {
        		g.drawImage(REDFactory, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof ScienceFacility) {
        		g.drawImage(REDLab, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof SolarPlant) {
        		g.drawImage(REDSolar, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof RecyclePlant) {
        		g.drawImage(REDRecycle, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof WaterCleaner) {
        		g.drawImage(REDWaterCleaner, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Nuclear) {
        		g.drawImage(REDNuclear, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2 - (int) (80*HEIGHTSCALE), (int) (162 * HEIGHTSCALE), (int) (162 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Landfill) {
        		g.drawImage(REDLandfill, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else if(mousedBuilding instanceof Tree) {
        		g.drawImage(REDTree, (int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE),  null);
        	}
        	else {
        		g.setColor(Color.RED);
        		g.fillRect((int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE));
        		//g.fillRect((int) ((mousedBuilding.x + 2*chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + 2*chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (5 * 16 * HEIGHTSCALE), (int) (5 * 16 * HEIGHTSCALE));
        		//g.fillRect(mousedBuilding.x, mousedBuilding.y, 100, 100);
        	}
        	if((mousedBuilding instanceof House || mousedBuilding instanceof Mine || mousedBuilding instanceof PowerPlant || mousedBuilding instanceof WaterCleaner|| mousedBuilding instanceof Nuclear)) {
            	g.setColor(Color.RED);
            	g.fillRect((int) ((mousedBuilding.x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((mousedBuilding.y + chunks.get(0).y + 5) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (16 * HEIGHTSCALE), (int) (100 * 16 * HEIGHTSCALE));
            }
        }
        
        
        if(co2Thingies.size() > 0) {
        	for(int i = 0; i < co2Thingies.size(); i++) {
        		//g.setColor(co2Thingies.get(i).color);
        		g.drawImage(CO2ThingyRED, (int) ((co2Thingies.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((co2Thingies.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (70 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		//g.drawString("CO2", (int) (co2Thingies.get(i).x), (int) (co2Thingies.get(i).y));
        	}
        }
        if(electrics.size() > 0) {
        	for(int i = 0; i < electrics.size(); i++) {
        		//g.setColor(co2Thingies.get(i).color);
        		g.drawImage(plusElectric, (int) ((electrics.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((electrics.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (50 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		//g.drawString("CO2", (int) (co2Thingies.get(i).x), (int) (co2Thingies.get(i).y));
        	}
        }
        if(faces.size() > 0) {
        	for(int i = 0; i < faces.size(); i++) {
        		if(faces.get(i) instanceof SmileyFace)
        			g.drawImage(SmileyFace, (int) ((faces.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((faces.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (50 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		if(faces.get(i) instanceof FrownyFace) {
        			g.drawImage(FrownyFace, (int) ((faces.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((faces.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (50 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		}
        	}
        }
        if(backwards.size() > 0) {
        	for(int i = 0; i < backwards.size(); i++) {
        		//g.setColor(co2Thingies.get(i).color);
        		g.drawImage(CO2ThingyGREEN, (int) ((backwards.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((backwards.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (70 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		//g.drawString("CO2", (int) (co2Thingies.get(i).x), (int) (co2Thingies.get(i).y));
        	}
        }
        if(munnythings.size() > 0) {
        	for(int i = 0; i < munnythings.size(); i++) {
        		//g.setColor(co2Thingies.get(i).color);
        		g.setFont(font4);
        		if(munnythings.get(i).identifier.charAt(0) == '+') {
        			g.setColor(Color.GREEN);
        		}
        		else {
        			g.setColor(Color.red);
        		}
        		//g.drawImage(CO2ThingyGREEN, (int) ((backwards.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((backwards.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2, (int) (70 * HEIGHTSCALE), (int) (50 * HEIGHTSCALE),  null);
        		g.drawString("" + munnythings.get(i).identifier, (int) ((munnythings.get(i).x + chunks.get(0).x) * 16 * HEIGHTSCALE) + WIDTH/2, (int) ((munnythings.get(i).y + chunks.get(0).y) * 16 * HEIGHTSCALE) + HEIGHT/2);
        		//g.drawString("CO2", (int) (co2Thingies.get(i).x), (int) (co2Thingies.get(i).y));
        	}
        }
        rainy += 20;
       	//g.drawImage(rainIMG, rainx, rainy, null);
      	g.drawImage(rainIMG, rainx + 1000, rainy, null);
      	g.drawImage(rainIMG, rainx + 2000, rainy, null);
       	g.drawImage(rainIMG, rainx, rainy - 600, null);
       	g.drawImage(rainIMG, rainx + 1000, rainy - 600, null);
       	g.drawImage(rainIMG, rainx + 2000, rainy - 600, null);
       	g.drawImage(rainIMG, rainx, rainy - 1200, null);
       	//g.drawImage(rainIMG, rainx - 1000, rainy, null);
       	//g.drawImage(rainIMG, rainx, rainy - 600, null);
       	//g.drawImage(rainIMG, rainx - 1000, rainy - 600, null);        	
       	//g.drawImage(rainIMG, rainx + 1000, rainy, null);
       	//g.drawImage(rainIMG, rainx + 100, rainy - 600, nul);
       	if(raining) {
       		if(rainx > 1000) {
       			rainx = 0;
       		}
       		if(rainy > 600) {
       			rainy = 0;
       		}
        }
        
        g.setFont(font);
        g.setColor(Color.BLACK);
        g.drawString("Money = $", 0, 20);
        if(munnies <= -250) {
        	g.setColor(Color.RED);
        }
        else if(munnies > -250 && munnies < 0) {
        	g.setColor(new Color(getColorFractionFromFraction(0, 255, ((-1*munnies)/250)), 0, 0));
        }
        else {
        	g.setColor(Color.BLACK);
        }
        g.drawString(df.format(munnies), 110, 20);
        g.setColor(Color.BLACK);
        g.drawString("Water:", 0, 80);
        //g.drawString("Fossil Fuels = " + fossilFyools, 0, 60);
        g.drawString("Science Level: " + scienceLevel, 0, 40);
        g.drawString("Population: " + population, 0, 60);
        //g.fillRect(10, 70, 120, 140);
        
        
        g.setColor(new Color(100, 100, 120));
        g.fillRoundRect(10, 90, 120, 140, 15, 15);
        g.setColor(new Color(120, 225, 255));
        g.fillRoundRect(10, 140+90-(int)(140*((double)waters/waterLimit)), 120, (int)(140*(((double)waters)/waterLimit)), 15, 15);
        
        
        //g.drawString("Pop growth: " + popDeriv, 0, 120);
        //g.drawString("Food per Capita: " + foodPerCapita, 0, 140);
        //g.drawString("Energy per Capita: " + energyPerCapita, 0, 160);
        //g.drawString("Foodings: " + foodings, 0, 180);
        //g.drawString("Happiness: " + happiness, 0, 200);
        
        /*if(buildings.size() >= 3 && System.currentTimeMillis() - startTime >= 45000) {
        	if(!tutorialMode || tutorialIndex >= tutStrings.size() - 1) {
        		for(int i = 0; i < warnings.size(); i++) {
        			g.drawString(warnings.get(i), WIDTH/2 - 110, 200 + 20*i);
        		}
        	}
        }*/
        
        //g.setColor(Color.black);
        //g.fillRect(WIDTH - 150, 0, 200, 75);
        g.drawImage(MenuMenuIMG, null, WIDTH - 100, 0);
      	g.drawImage(BuildingMenuIMG, null, WIDTH - 100, 100);
      	if(System.currentTimeMillis() - debtWarning <= 3000) {
      		g.setFont(font);
      		g.setColor(Color.black);
      		g.drawString("You have too much debt!", WIDTH/2 - 100, 150);
      	}
        //g.drawImage(StatBarsIMG, null, WIDTH - 100, 200);
      	g.setColor(Color.white);
      	g.fillRect(WIDTH - 100, 200, 100, 100);
        double popIntermediat = ((double)popCap)/((double)population);
		if(popIntermediat > 2) {
			popIntermediat = 2;
		}
		if(popIntermediat >= 1) {
			g.setColor(Color.green);
		}
		if(popIntermediat < 1) {
			try {
				g.setColor(new Color((int) (255 - (255*popIntermediat)), (int) (255 * popIntermediat), 0));
			} catch(Exception e) {System.out.println("poop12");}
		}
		g.fillRect(WIDTH - 100 + 8, (int) (300 - 8 - (37*popIntermediat)), 20, (int) (37*popIntermediat) + 1);
		
		double foodIntermediat = foodPerCapita/10;
		if(foodIntermediat > 2) {
			foodIntermediat = 2;
		}
		if(foodIntermediat >= 1) {
			g.setColor(Color.green);
		}
		if(foodIntermediat < 1) {
			try {
				g.setColor(new Color((int) (255 - (255*foodIntermediat)), (int) (255 * foodIntermediat), 0));
			} catch(Exception e) {System.out.println("poop13");}
		}
		g.fillRect(WIDTH - 100 + 30, (int) (300 - 8 - (37*foodIntermediat)), 20, (int) (37*foodIntermediat) + 1);
		
		double energyIntermediat = energyPerCapita/10;
		if(energyIntermediat > 2) {
			energyIntermediat = 2;
		}
		if(energyIntermediat >= 1) {
			g.setColor(Color.green);
		}
		if(energyIntermediat < 1) {
			try {
				g.setColor(new Color((int) (255 - (255*energyIntermediat)), (int) (255 * energyIntermediat), 0));
			} catch(Exception e) {System.out.println("poop14");}
		}
		if(energyIntermediat >= 0);
			g.fillRect(WIDTH - 100 + 52, (int) (300 - 8 - (37*energyIntermediat)), 20, (int) (37*energyIntermediat) + 1);
		
		double pollutionIntermediat = (100 - pollution)/50;
		double secondIntermediat = (2-pollutionIntermediat)/2;
		try {
			g.setColor(new Color((int) (255*(secondIntermediat)), 255 - ((int) (255*secondIntermediat)),0));
		} catch(Exception e) {System.out.println("poop15");}
		g.fillRect(WIDTH - 100 + 74, (int) (300 - 8 - (37*(2-pollutionIntermediat))), 20, (int) (37*(2-pollutionIntermediat)) + 1);
		
        //This is going to be the "pan area"
        //g.fillRect(0, 0, 20, HEIGHT);
        //g.setColor(new Color(0.75f, 0f, 1f, 1f));
        //g.fillRect(10, 100, (int) (100 * (science / ((gradReq + (scienceLevel - 1)*20)))), 10);
        //g.fillRect(10, 100, 100, 100);
        //gradReq + (scienceLevel - 1)*20
        
        g.setColor(Color.BLACK);
      	g.drawLine(WIDTH - 94, 208, WIDTH - 94, 292);
      	g.drawLine(WIDTH - 94, 292, WIDTH - 8, 292);
      	g.drawLine(WIDTH - 94, 293, WIDTH - 8, 293);
      	
      	if(mousedBuilding != null) {
        	g.setColor(Color.BLACK);
        	g.drawString("HINT: If you don't have space, press Esc", 100, 140);
        }
        
        try {
        	int starsToDraw = (happiness + 10)/10;
        	
        	int realStars = starsToDraw/2;
        	
        	if(System.currentTimeMillis() - lastStarRecord >= 1000) {
        		starRecord.add(realStars);
        		if(starRecord.size() > 10) {
        			starRecord.remove(0);
        		}
        		lastStarRecord = System.currentTimeMillis();
        	}
        	int toDraw = calcAverageStarCount();
        	
        	int pixelSpace = toDraw*70;
        	
        	for(int i = 0; i < toDraw; i++) {
        		g.drawImage(fullStar, WIDTH/2 - pixelSpace/2 + 70*i, 0, null);
        	}
        } catch (Exception e) {}
        
        try {
        	if(tutorialMode) {
        		if(tutorialIndex == 5 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 10, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 8 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 110, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 8 && !(guiCompo instanceof BuildingMenu)) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 11 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 210, HEIGHT - 220, null);
        			g.drawImage(downArrow, 410, HEIGHT - 220, null);
        			g.drawImage(downArrow, 610, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 11 && !(guiCompo instanceof BuildingMenu)) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 13) {
        			guiCompo = new BuildingMenu();
        			g.drawImage(downArrow, 310, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 4 || tutorialIndex == 7 || tutorialIndex == 10) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 14) {
        			g.drawImage(rightArrow, WIDTH - 210, 200, null);
        		}
        		else if(tutorialIndex == 16) {
        			g.drawImage(rightArrow, WIDTH - 210, 0, null);
        		}
        	}
        } catch(Exception e) {System.out.println("poop16");}
        
        if(guiCompo != null) {
        	if(guiCompo instanceof SaveLoader) {
        		g.setColor(Color.white);
        		g.fillRect(0, 0, guiCompo.y, HEIGHT);
        		g.setColor(Color.black);
        		for(int i = 0; i < fileInfo.size(); i++) {
                	if(fileInfo.get(i).grade.length() > 0) {
                		g.drawString("- " + fileInfo.get(i).name + ", Grade " + fileInfo.get(i).grade, 10, 20 + 20*i);
                	}
                	else {
                		g.drawString("- " + fileInfo.get(i).name, 10, 20 + 20*i);
                	}
                }
        	}
        	if(guiCompo instanceof DevModeMenu) {
        		g.setFont(font);
        		g.setColor(Color.white);
        		g.fillRect(WIDTH/2 - 250, HEIGHT/2-350, 500, 700);
        		g.setColor(Color.gray);
        		for(int i = 0; i < 7; i++) {
        			if(i == 0)
        				g.drawString("taxes, " + k[i], WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			if(i == 1) 
        				g.drawString("food, " + k[i], WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			if(i == 2) 
        				g.drawString("energy, " + k[i], WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			if(i == 3) 
        				g.drawString("pollution, " + k[i], WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			if(i == 4) 
        				g.drawString("water, " + k[i], WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			if(i == 5) {
        				g.drawString("amplitude(s), " + amplitude/1000, WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			}
        			if(i == 6) {
        				g.drawString("period(s), " + period/1000, WIDTH/2, HEIGHT/2 + 195 - 100*i + 100);
        			}
        			g.drawLine(WIDTH/2 - 200, HEIGHT/2 - 100*i + 300, WIDTH/2 + 200, HEIGHT/2 - 100*i + 300);
        			if(i < 5) {
        				g.fillRect((int) (WIDTH/2 - 200 + k[i]*4 - 5), HEIGHT/2 + 290 - 100*i, 10, 20);
        			}
        			if(i == 5) {
        				g.fillRect((int) (WIDTH/2 - 200 + amplitude*4/1000 - 5), HEIGHT/2 + 290 - 500, 10, 20);
        			}
        			if(i == 6) {
        				g.fillRect((int) (WIDTH/2 - 200 + period*2/10000 - 5), HEIGHT/2 + 290 - 600, 10, 20);
        			}
        		}
        	}
        	if(guiCompo instanceof MenuMenu) {
        		g.setFont(font);
        		g.setColor(new Color(255, 255, 255));
        		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        		g.fillRoundRect(WIDTH/2 - 210, HEIGHT/2 - 210, 420, 420, 50, 50);
        		//TAXES
        		g.setColor(Color.gray);
        		g.drawLine(WIDTH/2 - 200, HEIGHT/2 - 80, WIDTH/2 + 200, HEIGHT/2 - 80);
        		g.fillRect((int) (WIDTH/2 - 200 + taxes*4 - 5), HEIGHT/2 - 90, 10, 20);
        		FontMetrics fm = getFontMetrics(getFont());
        		int width = fm.stringWidth("Taxes");
        		g.setColor(Color.gray);
        		g.drawString("Taxes", WIDTH/2 - width, HEIGHT/2 - 150);
        		//WATER FOR HOUSES
        		g.setColor(Color.gray);
        		g.drawLine(WIDTH/2 - 200, HEIGHT/2, WIDTH/2 + 200, HEIGHT/2);
        		g.fillRect((int) (WIDTH/2 - 200 + houseWaterAllowance*4 - 5), HEIGHT/2 - 10, 10, 20);
        		FontMetrics fmh = getFontMetrics(getFont());
        		int widt = fm.stringWidth("Water for Houses");
        		g.setColor(Color.gray);
        		g.drawString("Water for Houses", WIDTH/2 - widt, HEIGHT/2 - 20);
        		//WATER FOR FARMS
        		g.setColor(Color.gray);
        		g.drawLine(WIDTH/2 - 200, HEIGHT/2 + 80, WIDTH/2 + 200, HEIGHT/2 + 80);
        		g.fillRect((int) (WIDTH/2 - 200 + farmWaterAllowance*4 - 5), HEIGHT/2 + 70, 10, 20);
        		FontMetrics fmth = getFontMetrics(getFont());
        		int wid = fm.stringWidth("Water for Farms");
        		g.setColor(Color.gray);
        		g.drawString("Water for Farms", WIDTH/2 - wid, HEIGHT/2 + 50);
        		g.setFont(font2);
        		//FontMetrics f = getFontMetrics(getFont());
        		//int widt = fm.stringWidth("(more taxes = more munnies, but more unhappy people)");
        		//g.drawString("(more taxes = more munnies, but more unhappy people)", WIDTH/2 - 185, HEIGHT/2 - 130);
        	}
        	else if(guiCompo instanceof StatsMenu) {
        		g.setColor(new Color(255, 255, 255));
        		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        		g.fillRoundRect(WIDTH/2 - 210, HEIGHT/2 - 210, 420, 420, 50, 50);
        		
        		double popIntermediate = ((double)popCap)/((double)population);
        		if(popIntermediate > 2) {
        			popIntermediate = 2;
        		}
        		if(popIntermediate >= 1) {
        			g.setColor(Color.green);
        		}
        		if(popIntermediate < 1) {
        			try {
        				g.setColor(new Color((int) (255 - (255*popIntermediate)), (int) (255 * popIntermediate), 0));
        			} catch(Exception e) {System.out.println("poop17");}
        		}
        		g.fillRect(WIDTH/2 - 185 + 5, (int) (HEIGHT/2 + 185 - (185*popIntermediate)), 86, (int) (185*popIntermediate) + 1);
        		
        		g.setColor(Color.BLACK);
        		g.setFont(mini);
        		g.drawString("housing", WIDTH/2 - 185 + 8, HEIGHT/2 + 185 + 15);
        		
        		double foodIntermediate = foodPerCapita/10;
        		if(foodIntermediate > 2) {
        			foodIntermediate = 2;
        		}
        		if(foodIntermediate >= 1) {
        			g.setColor(Color.green);
        		}
        		if(foodIntermediate < 1) {
        			try {
        				g.setColor(new Color((int) (255 - (255*foodIntermediate)), (int) (255 * foodIntermediate), 0));
        			} catch(Exception e) {System.out.println("poop18");}
        		}
        		g.fillRect(WIDTH/2 - 185 + 10 + 86, (int) (HEIGHT/2 + 185 - (185*foodIntermediate)), 86, (int) (185*foodIntermediate) + 1);
        		
        		g.setColor(Color.BLACK);
        		g.drawString("food", WIDTH/2 - 185 + 8 + 25 + 86, HEIGHT/2 + 185 + 15);
        		
        		double energyIntermediate = energyPerCapita/10;
        		if(energyIntermediate > 2) {
        			energyIntermediate = 2;
        		}
        		if(energyIntermediate >= 1) {
        			g.setColor(Color.green);
        		}
        		if(energyIntermediate < 1) {
        			try {
        				g.setColor(new Color((int) (255 - (255*energyIntermediate)), (int) (255 * energyIntermediate), 0));
        			} catch(Exception e) {System.out.println("poop19");}
        		}
        		g.fillRect(WIDTH/2 - 185 + 15 + 172, (int) (HEIGHT/2 + 185 - (185*energyIntermediate)), 86, (int) (185*energyIntermediate) + 1);
        		
        		g.setColor(Color.BLACK);
        		g.drawString("electricity", WIDTH/2 - 185 + 8 + 15 + 172, HEIGHT/2 + 185 + 15);
        		
        		double pollutionIntermediate = (100 - pollution)/50;
        		double secondIntermediate = (2-pollutionIntermediate)/2;
        		try {
        			g.setColor(new Color((int) (255*(secondIntermediate)), 255 - ((int) (255*secondIntermediate)),0));
        		} catch(Exception e) {System.out.println("poop20");}
        		g.fillRect(WIDTH/2 - 185 + 20 + 172 + 86, (int) (HEIGHT/2 + 185 - (185*(2-pollutionIntermediate))), 86, (int) (185*(2-pollutionIntermediate)) + 1);
        		g.setColor(Color.black);
        		g.drawString("CO2", WIDTH/2 - 185 + 8 + 40 + 86 + 172, HEIGHT/2 + 185 + 15);
        		
        		g.setColor(Color.black);
        		g.drawLine(WIDTH/2 - 185, HEIGHT/2 - 185, WIDTH/2 - 185, HEIGHT/2 + 185);
        		g.drawLine(WIDTH/2 - 185, HEIGHT/2 + 185, WIDTH/2 + 185, HEIGHT/2 + 185);
        	}
        	else if(guiCompo instanceof BuildingUpgradeMenu && buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof House) {
        		guiCompo.draw(g, WIDTH, HEIGHT);
        		g.setFont(font);
        		g.drawString("Transportation", WIDTH - 220, 150);
        		g.drawLine(WIDTH - 250, 180, WIDTH - 250, 280);
        		if(((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType == 0) {
        			g.drawString("Gasoline Car", WIDTH - 240, 200);
        		}
        		else if(((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType == 1) {
        			g.drawString("Electric Car", WIDTH - 240, 200);
        		}
        		else if(((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType == 2) {
        			g.drawString("Bus", WIDTH - 240, 200);
        		}
        		else {
        			g.drawString("Light Rail", WIDTH - 240, 200);
        		}
        		g.drawString("" + trannies.get(((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType).transDescription, WIDTH - 240, 250);
        		g.setColor(Color.GREEN);
        		g.fillRect(WIDTH - 210, 290, 90, 30);
        		g.setColor(Color.black);
        		g.drawString("Change", WIDTH - 200, 310);
        	}
        	else {
        		guiCompo.draw(g,  WIDTH, HEIGHT);
        	}
        }
        try {
        	if(tutorialMode) {
        		g.setColor(Color.BLACK);
        		g.setFont(font);
        		g.drawString(tutStrings.get(tutorialIndex), 10, 110);
        		/*if(tutorialIndex == 5 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 10, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 8 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 110, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 8 && !(guiCompo instanceof BuildingMenu)) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 11 && guiCompo instanceof BuildingMenu) {
        			g.drawImage(downArrow, 210, HEIGHT - 220, null);
        			g.drawImage(downArrow, 410, HEIGHT - 220, null);
        			g.drawImage(downArrow, 610, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 11 && !(guiCompo instanceof BuildingMenu)) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 13) {
        			guiCompo = new BuildingMenu();
        			g.drawImage(downArrow, 310, HEIGHT - 220, null);
        		}
        		else if(tutorialIndex == 4 || tutorialIndex == 7 || tutorialIndex == 10) {
        			g.drawImage(rightArrow, WIDTH - 210, 100, null);
        		}
        		else if(tutorialIndex == 14) {
        			g.drawImage(rightArrow, WIDTH - 210, 200, null);
        		}
        		else if(tutorialIndex == 16) {
        			g.drawImage(rightArrow, WIDTH - 210, 0, null);
        		}*/
        	}
        } catch(Exception e) {System.out.println("poop21");}
        if(guiCompo instanceof BuildingMenu) {
        	g.setFont(font);
        	if(mouseX >= 10 && mouseX <= 90) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("House", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 110 && mouseX <= 190) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Farm", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 210 && mouseX <= 290) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Fossil Fuel Power Plant", WIDTH/2 - 175, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 310 && mouseX <= 390) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Laboratory", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 410 && mouseX <= 490) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			if(scienceLevel >= 2 || !scienceMode) {
        				g.drawString(statPanelTag, 0, HEIGHT - 110);
        			}
        			else {
        				g.drawString("You need level 2 science!", 0, HEIGHT - 110);
        			}
        			//g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Solar Power Plant", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 510 && mouseX <= 590) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			if(scienceLevel >= 5 || !scienceMode) {
        				g.drawString(statPanelTag, 0, HEIGHT - 110);
        			}
        			else {
        				g.drawString("You need level 5 science!", 0, HEIGHT - 110);
        			}
        			//g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Recycling Plant", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 610 && mouseX <= 690) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			if(scienceLevel >= 15 || !scienceMode) {
        				g.drawString(statPanelTag, 0, HEIGHT - 110);
        			}
        			else {
        				g.drawString("You need level 15 science!", 0, HEIGHT - 110);
        			}
        			//g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Nuclear Power Plant", WIDTH/2 - 150, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 710 && mouseX <= 790) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Landfill", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 810 && mouseX <= 890) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			if(scienceLevel >= 10 || !scienceMode) {
        				g.drawString(statPanelTag, 0, HEIGHT - 110);
        			}
        			else {
        				g.drawString("You need level 10 science!", 0, HEIGHT - 110);
        			}
        			//g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Water Tank", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        	if(mouseX >= 910 && mouseX <= 990) {
        		if(mouseY >= HEIGHT - 110 && mouseY <= HEIGHT - 30) {
        			g.setColor(new Color(255, 255, 255));
        			g.fillRect(0, HEIGHT - 150, WIDTH, 30);
        			g.setColor(Color.black);
        			if(scienceLevel >= 10 || !scienceMode) {
        				g.drawString(statPanelTag, 0, HEIGHT - 110);
        			}
        			else {
        				g.drawString("You need level 25 science!", 0, HEIGHT - 110);
        			}
        			//g.drawString(statPanelTag, 0, HEIGHT - 110);
        			g.drawString("Tree", WIDTH/2 - 100, HEIGHT - 130);
        		}
        	}
        }
        
        g.dispose();
        bs.show();
    }

    public void keyTyped(KeyEvent e) {
        
    }

    public void keyPressed(KeyEvent e) {
    	if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
    		if(mousedBuilding != null) {
    			mousedBuilding = null;
    			guiCompo = null;
    		}
    		else if(guiCompo != null) {
    			guiCompo = null;
				files = file.listFiles();
			    fileInfo.clear();
    		}
    		else {
    			file = new File(".");
    			files = file.listFiles();
    			guiCompo = new SaveLoader();
    			for (int i = 0; i < files.length; i++) {
    	            if(files[i].toString().contains(".psave")) {
    	            	fileInfo.add(new FileInfo(files[i].toString()));
    	            }
    	        }
    			//load();
    			/*if(fileSave.length() < 1) {
    				fc = new FileChoose(this);
    			}
    			else {
    				load();
    			}*/
    			//System.out.println(bLog.split("x"));
    		}
    	}
    	if(state == GameState.GAME) {
    		if(e.getKeyChar() == 'd' || e.getKeyChar() == 'D' || e.getKeyCode() == KeyEvent.VK_RIGHT) {
    			for(int i = 0; i < chunks.size(); i++) {
    				rightHeld = true;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 'a' || e.getKeyChar() == 'A' || e.getKeyCode() == KeyEvent.VK_LEFT) {
    			for(int i = 0; i < chunks.size(); i++) {
    				leftHeld = true;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 'w' || e.getKeyChar() == 'W' || e.getKeyCode() == KeyEvent.VK_UP) {
    			for(int i = 0; i < chunks.size(); i++) {
    				upHeld = true;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 's' || e.getKeyChar() == 'S' || e.getKeyCode() == KeyEvent.VK_DOWN) {
    			for(int i = 0; i < chunks.size(); i++) {
    				downHeld = true;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 't' || e.getKeyChar() == 'T') {
    			zoomingIn = true;
    		}
    		if(e.getKeyChar() == 'g' || e.getKeyChar() == 'G') {
    			zoomingOut = true;
    		}
    		/*if(mousedBuilding != null) {
    			mousedBuilding = null;
    		}*/
    	}
    }

    public void keyReleased(KeyEvent e) {
    	if(e.getKeyCode() == KeyEvent.VK_SPACE) {
    		//save();
    		if(fileSave.length() < 1) {
    			ni = new NameInput(this);
        		ni.setVisible(true);
    		}
    		else {
    			save();
    		}
    	}
    	if(state == GameState.GAME) {
    		if((e.getKeyChar() == 'r' || e.getKeyChar() == 'R' ) && devMode) {
    			raining = !raining;
    			lastSwitch = System.currentTimeMillis();
    			rainx = -1000;
    			rainy = -600;
    		}
    		if(devMode && e.getKeyChar() == 'u') {
    			guiCompo = new DevModeMenu();
    		}
    		if(e.getKeyChar() == 'd' || e.getKeyChar() == 'D' || e.getKeyCode() == KeyEvent.VK_RIGHT) {
    			for(int i = 0; i < chunks.size(); i++) {
    				rightHeld = false;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 'a' || e.getKeyChar() == 'A' || e.getKeyCode() == KeyEvent.VK_LEFT) {
    			for(int i = 0; i < chunks.size(); i++) {
    				leftHeld = false;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 'w' || e.getKeyChar() == 'W' || e.getKeyCode() == KeyEvent.VK_UP) {
    			for(int i = 0; i < chunks.size(); i++) {
    				upHeld = false;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 's' || e.getKeyChar() == 'S' || e.getKeyCode() == KeyEvent.VK_DOWN) {
    			for(int i = 0; i < chunks.size(); i++) {
    				downHeld = false;
    				mousedBuildingCol = false;
    				checkMousedBuildingCol();
    			}
    		}
    		if(e.getKeyChar() == 't' || e.getKeyChar() == 'T') {
    			zoomingIn = false;
    		}
    		if(e.getKeyChar() == 'g' || e.getKeyChar() == 'G') {
    			zoomingOut = false;
    		}
    	}
    }

    public void actionPerformed(ActionEvent e) {
        
    }
    public void mousePressed(MouseEvent e) {
    	if(state == GameState.GAME) {
    		mouseX = e.getX();
    		mouseY = e.getY();
    		
    		if(guiCompo instanceof SaveLoader) {
    			if(mouseX > guiCompo.y) {
    				guiCompo = null;
    				File[] files = file.listFiles();
    			    fileInfo.clear();
    			}
    			else {
    				int x = (e.getY() / 20) + 1;
    		        try {
    		        	fileSave = fileInfo.get(x - 1).url;
    		        	load();
    		        } catch(Exception ie) {System.out.println(e);}
    			}
    		}
    		
    		/*for(int i = 0; i < buildings.size(); i++) {
    			if(buildings.get(i) instanceof PowerPlant) {
    				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
    					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
    						guiCompo = new BuildingUpgradeMenu(i);
    						return;
    					}
    				}
    			}
    			else if(buildings.get(i) instanceof Mine) {
    				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
    					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (5 * 16 * HEIGHTSCALE))) {
    						guiCompo = new BuildingUpgradeMenu(i);
    						return;
    					}
    				}
    			}
    			else if(buildings.get(i) instanceof Nuclear) {
    				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
    					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
    						guiCompo = new BuildingUpgradeMenu(i);
    						return;
    					}
    				}
    			}
    			else {
    				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (5 * 16 * HEIGHTSCALE))) {
    					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (5 * 16 * HEIGHTSCALE))) {
    						guiCompo = new BuildingUpgradeMenu(i);
    						if(tutorialIndex == 18) {
    							if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof House) {
    								tutorialIndex += 1;
    							}
    						}
    						if(tutorialMode && tutorialIndex == 2) {
    							tutorialIndex += 1;
    						}
    						return;
    					}
    				}
    			}
    		}*/
    		if(guiCompo instanceof DevModeMenu) {
    			if(mouseX <= WIDTH/2 - guiCompo.y/2 || mouseX >= WIDTH/2 + guiCompo.y/2 || mouseY <= HEIGHT/2 - guiCompo.y/2 || mouseY >= HEIGHT/2 + guiCompo.y/2) {
    				guiCompo = null;
    			}
    		}
    		if(guiCompo instanceof BuildingUpgradeMenu) {
    			if(mouseX <= WIDTH - guiCompo.y) {
    				guiCompo = null;
    				return;
    			}
    			if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof House) {
    				//g.fillRect(WIDTH - 210, 290, 90, 30);
    				if(mouseX <= WIDTH - 210 + 90 && mouseX >= WIDTH - 210) {
    					if(mouseY >= 290 && mouseY <= 320) {
    						((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType += 1;
    						if(((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType > 3) {
    							((House)buildings.get(((BuildingUpgradeMenu)guiCompo).reference)).transportationType = 0;
    						}
    						return;
    					}
    				}
    			}
    			if(mouseY < 100) {
    				//BUILDING REMOVE
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof PowerPlant) {
    					ffreq -= 2;
    					powerPlantCount -= 1;
    					rLog += "3";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof House) {
    					ffreq -= 1;
    					houseNumber -= 1;
    					rLog += "1";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof RecyclePlant) {
    					recycleCount -= 1;
    					rLog += "6";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof Tree) {
    					rLog += "0";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof Mine) {
    					rLog += "2";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof ScienceFacility) {
    					rLog += "4";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof SolarPlant) {
    					rLog += "5";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof Nuclear) {
    					rLog += "7";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof Landfill) {
    					rLog += "8";
    				}
    				if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof WaterCleaner) {
    					rLog += "9";
    					waterLimit -= 100;
    				}
    				munnythings.add(new MunnyParticle(buildings.get(((BuildingUpgradeMenu)guiCompo).reference).x, buildings.get(((BuildingUpgradeMenu)guiCompo).reference).y - 1, "-$10"));
    				buildings.remove(((BuildingUpgradeMenu)guiCompo).reference);
    				rLog += ":" + timeLine + "x";
    				//playDestroySound();
    				guiCompo = null;
    				munnies -= 10;
    				if(tutorialMode && tutorialIndex == 3) {
    					tutorialIndex += 1;
    				}
    				return;
    			}
    		}
    		if(mousedBuilding != null && !mousedBuildingCol) {
    			//playPlaceSound();
    			//int holder = (int) (((mouseX - WIDTH/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).x));
    			//holder -= holder%5;
    			//mousedBuilding.x = (int) (((mouseX - WIDTH/2) / (80 * HEIGHTSCALE)) - (chunks.get(0).x));
    			//mousedBuilding.x = holder;
    			//BUILDING ADD
    			if(munnies > -250) {
    				if(mousedBuilding instanceof Mine) {
    					((Mine) mousedBuilding).lastMine = System.currentTimeMillis();
    				}
    				if(mousedBuilding instanceof House) {
    					munnies -= 10;
    					buildings.add(mousedBuilding);
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$10"));
    					ffreq += 1;
    					houseNumber += 1;
    					population += 2;
    					bLog += "1";
    					faces.add(new SmileyFace(mousedBuilding.x + 1, mousedBuilding.y));
    					if(tutorialMode && tutorialIndex == 6) {
    						tutorialIndex += 1;
    					}
    				}
    				else if(mousedBuilding instanceof Mine) {
    					munnies -= 25;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$25"));
    					buildings.add(mousedBuilding);
    					bLog += "2";
    					if(foodPerCapita < 10) {
    						faces.add(new SmileyFace(mousedBuilding.x + 3, mousedBuilding.y));
    					}
    				}
    				else if(mousedBuilding instanceof PowerPlant) {
    					munnies -= 15;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$15"));
    					buildings.add(mousedBuilding);
    					ffreq += 2;
    					powerPlantCount += 1;
    					bLog += "3";
    					if(pollution > 50) {
    						faces.add(new FrownyFace(mousedBuilding.x + 3, mousedBuilding.y));
    					}
    					else if(energyPerCapita < 10) {
    						faces.add(new SmileyFace(mousedBuilding.x + 3, mousedBuilding.y));
    					}
    				}
    				else if(mousedBuilding instanceof ScienceFacility) {
    					munnies -= 30;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$30"));
    					buildings.add(mousedBuilding);
    					bLog += "5";
    				}
    				else if(mousedBuilding instanceof SolarPlant) {
    					munnies -= 60;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$60"));
    					buildings.add(mousedBuilding);
    					bLog += "5";
    					if(energyPerCapita < 10 || pollution > 0) 
    						faces.add(new SmileyFace(mousedBuilding.x + 1, mousedBuilding.y));
    				}	
    				else if(mousedBuilding instanceof RecyclePlant) {
    					munnies -= 30;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$30"));
    					buildings.add(mousedBuilding);
    					recycleCount += 1;
    					bLog += "6";
    					faces.add(new SmileyFace(mousedBuilding.x + 1, mousedBuilding.y));
    				}
    				else if(mousedBuilding instanceof Nuclear) {
    					munnies -= 60;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$60"));
    					buildings.add(mousedBuilding);
    					bLog += "7";
    					if(energyPerCapita < 10) 
    						faces.add(new SmileyFace(mousedBuilding.x + 1, mousedBuilding.y));
    				}
    				else if(mousedBuilding instanceof Landfill) {
    					munnies -= 30;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$30"));
    					buildings.add(mousedBuilding);
    					bLog += "8";
    				}
    				else if(mousedBuilding instanceof WaterCleaner) {
    					munnies -= 30;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$30"));
    					buildings.add(mousedBuilding);
    					bLog += "9";
    					waterLimit += 100;
    				}
    				else if(mousedBuilding instanceof Tree) {
    					munnies -= 50;
    					munnythings.add(new MunnyParticle(mousedBuilding.x, mousedBuilding.y - 1, "-$50"));
    					buildings.add(mousedBuilding);
    					bLog += "0";
    				}
    				bLog += ":" + timeLine + "x";
    			}
    			else {
    				debtWarning = System.currentTimeMillis();
    			}
    			mousedBuilding = null;
    			return;
    		}
    		if(guiCompo == null && e.getX() >= WIDTH - 100 && e.getY() >= 100 && e.getY() < 200) {
    			guiCompo = new BuildingMenu();
    			if(tutorialMode) {
    				if(tutorialIndex == 4 || tutorialIndex == 7 || tutorialIndex == 10) {
    					if(guiCompo instanceof BuildingMenu) {
    						tutorialIndex += 1;
    					}
    				}
    			}
    			return;
    		}
    		else if(guiCompo == null && e.getX() >= WIDTH - 100 && e.getY() < 100) {
    			guiCompo = new MenuMenu();
    			if(tutorialMode && tutorialIndex == 16) {
    				if(guiCompo instanceof MenuMenu) {
    					tutorialIndex += 1;
    				}
    			}
    			return;
    		}
    		else if(guiCompo == null && e.getX() >= WIDTH - 100 && e.getY() >= 200 && e.getY() < 300) {
    			guiCompo = new StatsMenu();
    			if(tutorialMode && tutorialIndex == 14) {
    				if(guiCompo instanceof StatsMenu) {
    					tutorialIndex += 1;
    				}
    			}
    			return;
    		}
    		else if(guiCompo instanceof MenuMenu) {
    			if(e.getX() < WIDTH/2 - 210 || e.getX() > WIDTH/2 + 210 || e.getY() < HEIGHT/2 - 210 || e.getY() > HEIGHT/2 + 210) {
    				guiCompo = null;
    			}
    		}
    		else if(guiCompo instanceof StatsMenu) {
    			if(e.getX() < WIDTH/2 - 210 || e.getX() > WIDTH/2 + 210 || e.getY() < HEIGHT/2 - 210 || e.getY() > HEIGHT/2 + 210) {
    				guiCompo = null;
    			}
    		}
    		else if(guiCompo instanceof BuildingMenu) {
    			if(e.getX() <= 90 && e.getX() >= 10) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new House();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 190 && e.getX() >= 110) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new Mine();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    				
    			}
    			if(e.getX() <= 290 && e.getX() >= 210) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new PowerPlant();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    				
    			}
    			if(e.getX() <= 390 && e.getX() >= 310) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new ScienceFacility();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 490 && e.getX() >= 410) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90 && (scienceLevel >= 2 || !scienceMode)) {
    					guiCompo = null;
    					mousedBuilding = new SolarPlant();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 590 && e.getX() >= 510 && (scienceLevel >= 5 || !scienceMode)) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new RecyclePlant();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 690 && e.getX() >= 610 && (scienceLevel >= 15 || !scienceMode)) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new Nuclear();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 790 && e.getX() >= 710) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
    					guiCompo = null;
    					mousedBuilding = new Landfill();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 890 && e.getX() >= 810) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90 && (scienceLevel >= 10 || !scienceMode)) {
    					guiCompo = null;
    					mousedBuilding = new WaterCleaner();
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			if(e.getX() <= 990 && e.getX() >= 910) {
    				if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90 && (scienceLevel >= 25 || !scienceMode)) {
    					guiCompo = null;
    					mousedBuilding = new Tree(0);
    					mousedBuilding.y = chunks.get(0).dirtLevel - 4;
    				}
    			}
    			else if(e.getY() < HEIGHT - 100){
    				guiCompo = null;
    			}
    		}
    	}
    	if(guiCompo == null) {
    	for(int i = 0; i < buildings.size(); i++) {
			if(buildings.get(i) instanceof PowerPlant) {
				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
						guiCompo = new BuildingUpgradeMenu(i);
						return;
					}
				}
			}
			else if(buildings.get(i) instanceof Mine) {
				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (5 * 16 * HEIGHTSCALE))) {
						guiCompo = new BuildingUpgradeMenu(i);
						return;
					}
				}
			}
			else if(buildings.get(i) instanceof Nuclear) {
				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (10 * 16.2 * HEIGHTSCALE))) {
						guiCompo = new BuildingUpgradeMenu(i);
						return;
					}
				}
			}
			else {
				if(mouseX >= getRealX(buildings.get(i).x) && mouseX <= getRealX(buildings.get(i).x) + ((int) (5 * 16 * HEIGHTSCALE))) {
					if(mouseY >= getRealY(buildings.get(i).y) && mouseY <= getRealY(buildings.get(i).y) + ((int) (5 * 16 * HEIGHTSCALE))) {
						guiCompo = new BuildingUpgradeMenu(i);
						if(tutorialIndex == 18) {
							if(buildings.get(((BuildingUpgradeMenu)guiCompo).reference) instanceof House) {
								tutorialIndex += 1;
							}
						}
						if(tutorialMode && tutorialIndex == 2) {
							tutorialIndex += 1;
						}
						return;
					}
				}
			}
		}
    	}
    	if(tutorialMode) {
			if(tutorialIndex == 4 || tutorialIndex == 7 || tutorialIndex == 10) {
				if(guiCompo instanceof BuildingMenu) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 14) {
				if(guiCompo instanceof StatsMenu) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 16) {
				if(guiCompo instanceof MenuMenu) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 17) {
				if(guiCompo == null) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 5) {
				if(mousedBuilding instanceof House) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 8) {
				if(mousedBuilding instanceof Mine) {
					tutorialIndex += 1;
				}
			}
			else if(tutorialIndex == 11) {
				if(mousedBuilding instanceof Nuclear || mousedBuilding instanceof SolarPlant || mousedBuilding instanceof PowerPlant) {
					tutorialIndex += 1;
				}
			}
			else {
				tutorialIndex += 1;
			}
		}
    }
    public void mouseReleased(MouseEvent e) {
    	/*if(mousedBuilding != null) {
    		//int holder = (int) (((mouseX - WIDTH/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).x));
    		//holder -= holder%5;
    		//mousedBuilding.x = (int) (((mouseX - WIDTH/2) / (80 * HEIGHTSCALE)) - (chunks.get(0).x));
    		//mousedBuilding.x = holder;
    		if(mousedBuilding instanceof Mine) {
    			((Mine) mousedBuilding).lastMine = System.currentTimeMillis();
    			
    		}
        	if(mousedBuilding instanceof House) {
        		munnies -= 10;
        		buildings.add(mousedBuilding);
        	}
        	else if(mousedBuilding instanceof Mine) {
        		munnies -= 25;
        		buildings.add(mousedBuilding);
        	}
        	else if(mousedBuilding instanceof PowerPlant) {
        		munnies -= 15;
        		buildings.add(mousedBuilding);
        	}
        	else if(mousedBuilding instanceof ScienceFacility) {
        		munnies -= 30;
        		buildings.add(mousedBuilding);
        	}
        	else if(mousedBuilding instanceof SolarPlant) {
        		munnies -= 30;
        		buildings.add(mousedBuilding);
        	}
        	mousedBuilding = null;
        	return;
        }
        if(guiCompo == null && e.getX() >= WIDTH - 100 && e.getY() >= 100) {
        	guiCompo = new BuildingMenu();
        }
        else if(guiCompo == null && e.getX() >= WIDTH - 100 && e.getY() < 100) {
        	guiCompo = new MenuMenu();
        }
        else if(guiCompo instanceof MenuMenu) {
        	if(e.getX() < WIDTH/2 - 210 || e.getX() > WIDTH/2 + 210 || e.getY() < HEIGHT/2 - 210 || e.getY() > HEIGHT/2 + 210) {
        		guiCompo = null;
        	}
        }
        else if(guiCompo instanceof BuildingMenu) {
        	if(e.getX() <= 90 && e.getX() >= 10) {
        		if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
        			guiCompo = null;
        			mousedBuilding = new House();
        		}
        		
        	}
        	if(e.getX() <= 190 && e.getX() >= 110) {
        		if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
        			guiCompo = null;
        			mousedBuilding = new Mine();
        		}
        		
        	}
        	if(e.getX() <= 290 && e.getX() >= 210) {
        		if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
        			guiCompo = null;
        			mousedBuilding = new PowerPlant();
        		}
        		
        	}
        	if(e.getX() <= 390 && e.getX() >= 310) {
        		if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
        			guiCompo = null;
        			mousedBuilding = new ScienceFacility();
        		}
        		
        	}
        	if(e.getX() <= 490 && e.getX() >= 410) {
        		if(e.getY() <= HEIGHT - 10 && e.getY() >= HEIGHT - 90) {
        			guiCompo = null;
        			mousedBuilding = new SolarPlant();
        		}
        		
        	}
        	else if(e.getY() < HEIGHT - 100){
        		guiCompo = null;
        	}
        }*/
    }
    public void mouseClicked(MouseEvent e) {
        if(state == GameState.MENU) {
        	if(e.getX() < WIDTH - 100) {
        		state = GameState.GAME;
        		if(e.getX() >= WIDTH/2 - 275 && e.getX() <= WIDTH/2 + 275) {
        			if(e.getY() >= HEIGHT/2 + 100 && e.getY() <= HEIGHT/2 + 200) {
        				tutorialMode = true;
        				//save();
        			}
        		}
        	}
        	else {
        		//FileChoose fc = new FileChoose(this);
        	}
        }
    }
    public void mouseEntered(MouseEvent e) {
        
    }
    public void mouseExited(MouseEvent e) {
        
    }

    public static void main(String args[]) {
        new Main().start();
    }

	public void mouseDragged(MouseEvent e) {
		if(state == GameState.GAME) {
			if(guiCompo instanceof MenuMenu) {
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 - 90 && e.getY() <= HEIGHT/2 - 70) {
						taxes = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 - 10 && e.getY() <= HEIGHT/2 + 10) {
						houseWaterAllowance = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 70 && e.getY() <= HEIGHT/2 + 90) {
						farmWaterAllowance = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
			}
			
			//HEIGHT/2 + 290 - 100*i
			/*
			 * if(i == 5) {
        				g.fillRect((int) (WIDTH/2 - 200 + amplitude*4/1000 - 5), HEIGHT/2 + 290 - 500, 10, 20);
        			}
        			if(i == 6) {
        				g.fillRect((int) (WIDTH/2 - 200 + period*2/10000 - 5), HEIGHT/2 + 290 - 600, 10, 20);
        			}
			 */
			if(guiCompo instanceof DevModeMenu) {
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 && e.getY() <= HEIGHT/2 + 310) {
						k[0] = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 100 && e.getY() <= HEIGHT/2 + 310 - 100) {
						k[1] = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 200 && e.getY() <= HEIGHT/2 + 310 - 200) {
						k[2] = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 300 && e.getY() <= HEIGHT/2 + 310 - 300) {
						k[3] = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 400 && e.getY() <= HEIGHT/2 + 310 - 400) {
						k[4] = (e.getX() - (WIDTH/2 - 200))/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 500 && e.getY() <= HEIGHT/2 + 310 - 500) {
						amplitude = ((e.getX() - (WIDTH/2 - 200))*1000)/4;
					}
				}
				if(e.getX() >= WIDTH/2 - 200 && e.getX() <= WIDTH/2 + 200) {
					if(e.getY() >= HEIGHT/2 + 290 - 600 && e.getY() <= HEIGHT/2 + 310 - 600) {
						period = ((e.getX() - (WIDTH/2 - 200))*10000)/2;
					}
				}
			}
		}
	}

	public void mouseMoved(MouseEvent e) {
		if(state == GameState.GAME) {
			mouseX = e.getX();
			mouseY = e.getY();
			if(mousedBuilding != null) {
				//mousedBuilding.x = mouseX;
				//mousedBuilding.y = mouseY;
				mousedBuilding.x = (int) (((mouseX - WIDTH/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).x));
				if(mousedBuilding.x < 0) {
					mousedBuilding.x = 0;
				}
				if(mousedBuilding instanceof Mine || mousedBuilding instanceof PowerPlant || mousedBuilding instanceof Nuclear) {
					if(mousedBuilding.x > chunks.get(0).blocks.length - 10) {
						mousedBuilding.x = chunks.get(0).blocks.length - 10;
					}
				}
				else {
					if(mousedBuilding.x > chunks.get(0).blocks.length - 5) {
						mousedBuilding.x = chunks.get(0).blocks.length - 5;
					}
				}
				checkMousedBuildingCol();
				//mousedBuilding.y = (int) (((mouseY - HEIGHT/2) / (16 * HEIGHTSCALE)) - (chunks.get(0).y));
				mousedBuilding.y = chunks.get(0).dirtLevel - 4;
			}
			
			//STATPANEL TAG
			if(guiCompo instanceof BuildingMenu) {
				if(mouseX >= 10 && mouseX <= 90) {
	        		if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
	        			statPanelTag = "+Money, -Electricity, -Food, Cost: $10, *Place over water*";
	        		}
				}
				if(mouseX >= 110 && mouseX <= 190) {
	        		if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
	        			statPanelTag = "+Food, Cost: $25, *Place over water*";
	        		}
				}
				if(mouseX >= 210 && mouseX <= 290) {
	        		if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
	        			statPanelTag = "+Electricity, +Pollution, Cost: $15, *Place over fossil fuels*";
	        		}
				}
				if(mouseX >= 310 && mouseX <= 390) {
	        		if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
	        			statPanelTag = "-Money, -Electricity, +Science, Cost: $30";
	        		}
				}
				if(mouseX >= 410 && mouseX <= 490) {
	        		if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
	        			statPanelTag = "+Electricity, +Money, Cost: $60";
	        		}
				}
				if(mouseX >= 510 && mouseX <= 590) {
					if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
						statPanelTag = "-Electricity, -Pollution, Cost: $30, *Keeps houses from polluting water*";
					}
				}
				if(mouseX >= 610 && mouseX <= 690) {
					if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
						statPanelTag = "+Electricity, Cost: $60, *Place over water*";
					}
				}
				if(mouseX >= 710 && mouseX <= 790) {
					if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
						statPanelTag = "-Pollution, Cost: $30, *Place over water*";
					}
				}
				if(mouseX >= 810 && mouseX <= 890) {
					if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
						statPanelTag = "-Electricity, Cost: $30, *Lets you keep more water in your tank*";
					}
				}
				if(mouseX >= 910 && mouseX <= 990) {
					if(mouseY >= HEIGHT - 90 && mouseY <= HEIGHT - 10) {
						statPanelTag = "Cost: $50, *TREEEEEES!*";
					}
				}
			}
		}
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches = e.getWheelRotation();
	    if(notches < 0) {
	    	if(THEOHEIGHTSCALE < MAXZOOM) {
        		THEOHEIGHTSCALE += 0.02;
	    	}
	    }
	    if(notches > 0) {
	    	if(THEOHEIGHTSCALE > MINZOOM) {
	    		THEOHEIGHTSCALE -= 0.02;
	    	}
	    }
	}
}