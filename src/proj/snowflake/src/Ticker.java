package proj.snowflake.src;

public class Ticker implements Runnable {
	
	Main main;
	
	public long lastTick;
	
	public Ticker(Main m) {
		main = m;
	}
	
	public void tick() {
		long yolo1 = System.currentTimeMillis() - lastTick;
        lastTick = System.currentTimeMillis();
    }
	
	public void run() {
		/*while(true) {
			if(System.currentTimeMillis() - lastTick >= 1000/90) {
				main.tick();
				tick();
			}
		}*/
		while(true) {
		for(int i = 0; i < main.chunks.get(0).blocks.length; i++) {
			for(int j = 0; j < main.chunks.get(0).blocks[0].length; j++) {
				try {
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i+1][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+1][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i+1][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+1][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i-1][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-1][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-1][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i-1][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-1][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-1][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-1][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i+2][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+2][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+2][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+2][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i+2][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+2][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+2][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+2][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i-2][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-2][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-2][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-2][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i-2][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-2][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-2][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-2][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i+3][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+3][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+3][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+3][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i+3][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+3][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i+3][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i+1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i+3][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					
					
					if((main.chunks.get(0).blocks[i][j] instanceof AquiferBlock)) {
						if(main.chunks.get(0).blocks[i-3][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-3][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-3][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-3][j+1] = new AquiferBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
					if((main.chunks.get(0).blocks[i][j] instanceof DirtyWaterBlock)) {
						if(main.chunks.get(0).blocks[i-3][j+1] instanceof DiscoFFBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-3][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DiscoFFBlock();
						}
						if(main.chunks.get(0).blocks[i-3][j+1] instanceof DarkStoneBlock || main.chunks.get(0).blocks[i-1][j+1] instanceof DarkDirtBlock) {
							main.chunks.get(0).blocks[i-3][j+1] = new DirtyWaterBlock();
							main.chunks.get(0).blocks[i][j] = new DarkStoneBlock();
						}
					}
				} catch(Exception e) {}
			}
		}
		}
	}
}
