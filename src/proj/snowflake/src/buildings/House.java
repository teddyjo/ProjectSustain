package proj.snowflake.src.buildings;

public class House extends Building {
	
	public long lastTick;
	public long lastMine;
	public long lastPollute;
	
	public int mineIndex = 0;
	public boolean watFound = false;
	
	public int transportationType = 0;
	
	public House() {
		lastTick = System.currentTimeMillis();
		lastMine = System.currentTimeMillis();
	}
}
