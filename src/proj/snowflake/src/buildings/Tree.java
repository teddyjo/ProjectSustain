package proj.snowflake.src.buildings;

public class Tree extends Building {
	public long lastSequestration;
	
	public long sequestrationTime;
	
	public int treeType;
	
	public Tree(int x) {
		lastSequestration = System.currentTimeMillis();
		treeType = x;
		sequestrationTime = ((int) (Math.random()* 2000)) + 3000;
	}
}
