package proj.snowflake.transporters;

public class Bus extends Transporter {
	public Bus(double x, double y, double destination) {
		super(x, y, destination);
		this.transDescription = "-1 fossil fuel";
	}
}
