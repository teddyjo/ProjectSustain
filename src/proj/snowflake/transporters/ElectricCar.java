package proj.snowflake.transporters;

public class ElectricCar extends Transporter {
	public ElectricCar(double x, double y, double destination) {
		super(x, y, destination);
		this.transDescription = "-3 energy";
	}
}
