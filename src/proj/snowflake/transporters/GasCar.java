package proj.snowflake.transporters;

public class GasCar extends Transporter {
	public GasCar(double x, double y, double destination) {
		super(x, y, destination);
		this.transDescription = "-3 fossil fuels";
	}
}
