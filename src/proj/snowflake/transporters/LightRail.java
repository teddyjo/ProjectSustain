package proj.snowflake.transporters;

public class LightRail extends Transporter {
	public LightRail(double x, double y, double destination) {
		super(x, y, destination);
		this.transDescription = "-1 energy";
	}
}
