package proj.snowflake.transporters;

public class Transporter {
	public double x;
	public double y;
	public double destinationx;
	
	public long lastMove;
	
	public String transDescription = "";
	
	public boolean goingLeft;
	
	public Transporter(double x, double y, double destination) {
		this.x = x;
		this.y = y;
		this.destinationx = destination;
		
		if(destination - x < 0) {
			goingLeft = true;
		}
		else {
			goingLeft = false;
		}
		
		lastMove = System.currentTimeMillis();
	}
}
